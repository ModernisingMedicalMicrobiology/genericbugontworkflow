# Use an official Python runtime as a parent image
FROM ubuntu:20.04
from continuumio/miniconda
RUN conda config --add channels defaults && \
    conda config --add channels bioconda && \
    conda config --add channels conda-forge

COPY environment.yml /
RUN conda env create -f environment.yml  && \
	conda init bash

ENV PATH /opt/conda/envs/genericBugWorkflow/bin:$PATH

RUN apt-get --allow-releaseinfo-change update && apt-get install -y procps

RUN pypy3 -m ensurepip && pypy3 -m pip install --no-cache-dir intervaltree==3.0.2
