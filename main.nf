#!/usr/bin/env nextflow

// enable dsl2
nextflow.enable.dsl=2

// include subworkflows
include {prep} from './subworkflow/prep.nf'                                     
include {mapping; mapping as geneMapping} from './subworkflow/mapping.nf'                                     
include {geneAssemblies} from './subworkflow/assembly.nf'                                     
include {analysis} from './subworkflow/analysis.nf'                                     
include {chrom_analysis; chrom_analysis as gene_analysis} from './subworkflow/chrom_analysis.nf'     
include {mobile_analysis} from './subworkflow/mobile_analysis.nf'
include {report_wf} from './subworkflow/report.nf'

// ########################### MAIN ##########################################//
workflow {
//############################ channels ######################################//
Channel
	.fromPath( "${params.ref}" )
        .map{ file -> tuple(file.baseName, file) }
	.set{ refinputs } 


if (params.genes != ''){
Channel
        .fromPath( "${params.genes}" )
        .map{ file -> tuple(file.baseName, file) }                              
        .set{ genes }
}

if (params.centdb != ''){
Channel
	.fromPath( "${params.centdb}" )
        .map{ file -> tuple(file.baseName, file.getParent(), file) }
	.set{ centdb1 } 
}
else {
    centdb1 = Channel.from( params.dlCentrifugeindex )
}

if ("${params.infastq}".endsWith('.csv')){
    Channel
	.fromPath( "${params.infastq}" )
        .splitCsv()
        .map{ row -> tuple(row[0], file(row[1])) }
	.set{ runs }
}
else {
Channel                                                                         
       .fromPath( "${params.infastq}/*" )                                        
       .map{ file -> tuple(file.simpleName, file) }                      
       .set{ runs }
}

Channel                                                                         
        .fromPath( "${params.CLAIRMODEL}" )
        .set{ clairChannel }

Channel                                                                         
        .fromPath( "${params.modelBase}" )
        .set{ modelbase }

if (params.geneBed != '') {
Channel
        .fromPath( "${params.geneBed}" )                                      
        .set{ geneBed } 
}

if (params.mutations != '') {
Channel
        .fromPath( "${params.mutations}" )                                      
        .set{ mutations } 
}

if (params.chromosome_determinants != '') {                                         
Channel
        .fromPath( "${params.chromosome_determinants}" )                                      
        .set{ chromosome_determinants } 
}

if (params.mobile_determinants != '') {  
Channel
        .fromPath( "${params.mobile_determinants}" )                                      
        .set{ mobile_determinants } 
}
if (params.plasmids != '') {  
channel
        .fromPath( "${params.plasmids}" )                                      
        .map{ file -> tuple(file.simpleName, file) }                      
        .set{ plasmids } 
}
if (params.plasmid_genes != '') {  
channel
        .fromPath( "${params.plasmid_genes}" )                                      
        .map{ file -> tuple(file.simpleName, file) }                      
        .set{ plasmid_genes } 
}

// ########################### workfow ########################################//
    main:
        // ## if metagenomic samples, extract on taxid first
        if (params.taxid != '') {
                prep(runs,centdb1)
                map_fqs = prep.out.fastqs
        }
        else {
                map_fqs = runs
        }

        // ## mapping workflow
        
        mapping(map_fqs, refinputs, clairChannel, modelbase)
 

        // ## Analysis
        if (params.genes != '') { 
        if (params.plasmids != '') { 
                geneAssemblies( map_fqs, runs, genes, plasmid_genes, plasmids )
        }
        else {
                geneAssemblies( map_fqs, runs, genes, null, null)
        }
        }

        if (params.chromosome_determinants != '') { 
        // ## split analysis into chrom and mobile so chrom can be repeated
        chrom_analysis(mapping.out.masked, 
                mapping.out.pysamstats, 
                geneBed, 
                mutations, 
                chromosome_determinants,
                mapping.out.bamfiles)
        }

        if (params.mobile_determinants != '') {
        // ## collect file outputs from gene chrom analysis and gene analysis
        geneMapping(geneAssemblies.out.geneFastqs, genes, clairChannel, modelbase)

        gene_analysis(geneMapping.out.masked,
                geneMapping.out.pysamstats,
                geneBed,
                mutations,
                chromosome_determinants,
                geneMapping.out.bamfiles)

        }

        if (params.chromosome_determinants != '') {
        // ## colbine chrom results
        chrom_analysis.out.SR.combine(gene_analysis.out.SR, by:0)
                .collectFile(name:'chrom_SR.csv',
                sort:true, newLine: false, keepHeader:true)
                .map{ file -> tuple(file.baseName, file)}
                .set{ chrom_SR }

        chrom_analysis.out.results.combine(gene_analysis.out.results, by:0)
                .collectFile(name:'chrom_results.csv',
                sort:true, newLine: false, keepHeader:true)
                .map{ file -> tuple(file.baseName, file)}
                .set{ chrom_results }

        chrom_analysis.out.abx.combine(gene_analysis.out.abx, by:0)
                .collectFile(name:'chrom_abx.csv',
                sort:true, newLine: false, keepHeader:true)
                .map{ file -> tuple(file.baseName, file)}
                .set{ chrom_abx }

        chrom_analysis.out.geneCovs.combine(gene_analysis.out.geneCovs, by:0)
                .collectFile(name:'chrom_geneCovs.csv',
                sort:true, newLine: false, keepHeader:true)
                .map{ file -> tuple(file.baseName, file)}
                .set{ chrom_geneCovs }
        }

        if (params.mobile_determinants != '') {
        // ## mobile presence/absence only analysis
        mobile_analysis(geneAssemblies.out.Accdepth,
                mobile_determinants,
                mapping.out.genomeDepths, 
                geneAssemblies.out.plasmidBlast,
                geneAssemblies.out.geneBlast)
        }

        if (params.chromosome_determinants != '') {
        // ## reports
        report_wf(chrom_SR,
                chrom_results,
                chrom_abx,
                chrom_geneCovs,
                mobile_analysis.out.SR,
                mobile_analysis.out.results,
                mapping.out.genomeDepths,
                chromosome_determinants,
                mobile_determinants)
        }

}

