sudo docker run  \
	-v /var/run/docker.sock:/var/run/docker.sock  \
	-v $PWD/sing_images:/output  \
	--privileged -t --rm  \
	singularityware/docker2singularity:v2.5 \
	genericbugontworkflow
