// include modules                                                                                     
include {report} from '../modules/analysis.nf'                         
include {mutation_report} from '../modules/analysis.nf'                         
    
// analysis                                                                     
workflow report_wf{                                                              
    take:                                                                       
        chrom_SR
        chrom_results
        chrom_abx
        chrom_geneCovs
        mobile_SR
        mobile_results
        genomeDepths
        chromosome_determinants
        mobile_determinants
                                                                                
    main:
        // ## combined report for chrom and mobile
        report(mobile_SR.combine(chrom_SR, by:0)
            .combine(genomeDepths, by:0))

        report.out.tsv.collectFile(name:'sensitivity_report.tsv',
            storeDir:"report" ,
            keepHeader:true,
            skip:1) 

        mutation_report(mobile_results.combine(chrom_results, by:0)
                    .combine(chrom_abx, by:0)
                    .combine(chrom_geneCovs, by:0)
                    .combine(chromosome_determinants)         
                    .combine(mobile_determinants) )         
                                                                              
        mutation_report.out.tsv.collectFile(name:'gene_mutation_report.tsv',
            storeDir:"report" ,
            keepHeader:true,
            skip:1) 
}
