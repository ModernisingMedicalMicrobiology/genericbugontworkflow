include {overlapAccGenes} from '../modules/assemblies.nf'                      
include {binAccGenes} from '../modules/assemblies.nf'                          
include {overlapAccGenes as overlapAccPlasmids} from '../modules/assemblies.nf'
include {binAccGenes as binAccPlasmids} from '../modules/assemblies.nf'
include {mapAccGenes} from '../modules/assemblies.nf'                          
include {accgenedepth} from '../modules/assemblies.nf'                         
include {assembleAccessory} from '../modules/assemblies.nf'   
include {makeblastdb as makeblastdbPLASMIDS} from '../modules/assemblies.nf'   
include {makeblastdb as makeblastdbGENES} from '../modules/assemblies.nf'   
include {blast as blastPlasmids} from '../modules/assemblies.nf'   
include {blast as blastGenes} from '../modules/assemblies.nf'   


// gene assemblies                                                              
workflow geneAssemblies{                                                        
    take:                       
        species_fastqs                                                
        fastqs                                                                  
        genes
        plasmid_genes
        plasmids
                                                                                
    main:            
        // ######## chromosome genes ##############                                                           
        overlapAccGenes( genes.combine(species_fastqs) )                                
                                                                                
        binAccGenes( overlapAccGenes.out.genePafs.combine(species_fastqs, by:0) )       
                                                                                
        mapAccGenes(binAccGenes.out.binnedAccGenes)                             
                                                                                
        accgenedepth( mapAccGenes.out.mapGenes )   

        // ########## plasmid genes ################

        if (params.plasmid_genes != '') {
        overlapAccPlasmids(plasmid_genes.combine(fastqs))                              
                                
        binAccPlasmids( overlapAccPlasmids.out.genePafs.combine(fastqs, by:0) ) 
                                           
        assembleAccessory( binAccPlasmids.out.binnedUTAccGenes )                  
    
        makeblastdbPLASMIDS(plasmids)

        makeblastdbGENES(plasmid_genes)

        blastPlasmids(assembleAccessory.out.contigs.combine(makeblastdbPLASMIDS.out))

        blastGenes(assembleAccessory.out.contigs.combine(makeblastdbGENES.out))
        
        plasmidBlast = blastPlasmids.out.blast06

        geneBlast = blastGenes.out.blast06
        }
        else {

            plasmidBlast = ''
            geneBlast = ''

        }

        emit:

        Accdepth = accgenedepth.out.Accdepth 

        plasmidBlast = plasmidBlast

        geneBlast = geneBlast

        geneFastqs = binAccGenes.out.binnedAccGenesNoRef                                                                               
}                                                                               
