include {classify} from '../modules/prep.nf'                                     
include {binReads} from '../modules/prep.nf'         

// prep workflow                                             
workflow prep{                                                                  
    take:                                                                       
        runs                                                                    
        centdb1                                                                 
    main:                                                                       
        classify(runs.combine(centdb1))                                         
                
        binReads(classify.out.centFiles.combine(runs))                                                                
    emit:
        binReads.out.fastqs                                                                    
}                                                      
                                                                                
//// mapping                                                                      
//workflow mapping{                                                               
//    take:                                                                       
//        fastqs                                                                  
//        refinputs                                                               
//        clairChannel                                                            
//        modelbase                                                               
//                                                                                
//    main:                                                                       
//        map(refinputs.combine(fastqs))                                          
//                                                                                
//        pysamStats(map.out.fullAligned)                                         
//                                                                                
//        clair(map.out.fullAligned2.combine(clairChannel))                       
//                                                                                
//        makeGenomeClairConensus(clair.out.clairVCFs.combine(pysamStats.out.ch, by:0).combine(modelbase) )
//                                                                                
//        genomeDepth(map.out.aligned)                                            
//                                                                                
//    emit:                                                                       
//                                                                                
//}                                                                               
//                                                                                
//// gene assemblies                                                              
//workflow geneAssemblies{                                                        
//    take:                                                                       
//        fastqs                                                                  
//        genes                                                                   
//                                                                                
//    main:                                                                       
//        overlapAccGenes( genes.combine(fastqs) )                                
//                                                                                
//        binAccGenes( overlapAccGenes.out.genePafs.combine(fastqs, by:0) )       
//                                                                                
//        mapAccGenes(binAccGenes.out.binnedAccGenes)                             
//                                                                                
//        accgenedepth( mapAccGenes.out.mapGenes )                                
//                                                                                
//        assembleAccessory( binAccGenes.out.binnedUTAccGenes )                   
//                                                                                
//    emit:                                                                       
//                                                                                
//                                                                                
//}                                                                               
//                                                                                
//// analysis                                                                     
//workflow analysis{                                                              
//    take:                                                                       
//        unmasked                                                                
//        pysamstats                                                              
//        geneBed                                                                 
//        mutations                                                               
//        chromosome_determinants                                                 
//        Accdepth                                                                
//        mobile_determinants                                                     
//                                                                                
//    main:                                                                       
//        refVariants(unmasked.combine(pysamstats, by:0).combine(geneBed).combine(mutations))
//                                                                                
//        analyse_chrom(refVariants.out.allrefVariants.combine(chromosome_determinants))
//                                                                                
//        analyse_mobile(Accdepth.combine(mobile_determinants))                   
//    emit:                                                                       
//                                                                                
//}
