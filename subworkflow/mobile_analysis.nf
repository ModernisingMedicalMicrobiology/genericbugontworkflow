// include modules                                                              
include {refVariants} from '../modules/analysis.nf'                          
include {indel_class} from '../modules/analysis.nf'                         
include {analyse_chrom} from '../modules/analysis.nf'                          
include {analyse_mobile} from '../modules/analysis.nf'                         
include {analyse_plasmid} from '../modules/analysis.nf'                         
include {report} from '../modules/analysis.nf'                         
include {mutation_report} from '../modules/analysis.nf'                         
    
// analysis                                                                     
workflow mobile_analysis{                                                              
    take:                                                                                                                    
        Accdepth                                                                
        mobile_determinants
        genomeDepths 
        plasmidBlast
        geneBlast  
                                                                                
    main:
        // ## analysis of mobile determinants
        if (params.plasmids != '') { 
            analyse_plasmid(plasmidBlast.combine(geneBlast,by:0))

            Accdepth.combine(analyse_plasmid.out.csv, by:0)
                    .collectFile(name:'mobile_depths.csv', 
                    sort: true, newLine: true, keepHeader:true)
                    .map{ file -> tuple(file.baseName, file) }
                    .set{ Accdepth_combined }
        }
        else {
            Accdepth_combined = Accdepth
        }
        
        analyse_mobile(Accdepth_combined
            .combine(mobile_determinants)
            .combine(genomeDepths, by:0))

        emit:
        SR = analyse_mobile.out.SR
        results = analyse_mobile.out.mobileResults
}
