// include modules                                                              
include {refVariants} from '../modules/analysis.nf'                          
include {indel_class} from '../modules/analysis.nf'                         
include {analyse_chrom} from '../modules/analysis.nf'                          
include {analyse_mobile} from '../modules/analysis.nf'                         
include {analyse_plasmid} from '../modules/analysis.nf'                         
include {report} from '../modules/analysis.nf'                         
include {mutation_report} from '../modules/analysis.nf'                         
    
// analysis                                                                     
workflow chrom_analysis{                                                              
    take:                                                                       
        masked                              
        pysamstats                                                              
        geneBed                                                                 
        mutations                                                               
        chromosome_determinants                                                 
        bamfiles

                                                                                
    main:
        // ## analysis of chromosome determinants from fasta and bam files                                                                       
        refVariants(masked.combine(pysamstats, by:0).combine(geneBed).combine(mutations))

        indel_class(bamfiles.combine(mutations).combine(geneBed))

        // ## combine into a single file
        refVariants.out.allrefVariants.combine(indel_class.out.indels_csv, by:0)
                    .collectFile(name:'chrom_variants.csv', 
                    sort: true, newLine: true, keepHeader:true)
                    .map{ file -> tuple(file.baseName, file) }
                    .set{ chrom_collected }
                                                                                
        analyse_chrom(chrom_collected
                    .combine(refVariants.out.geneCovs, by:0)
                    .combine(chromosome_determinants))

        emit:
        SR = analyse_chrom.out.SR
        abx= analyse_chrom.out.abx
        geneCovs=analyse_chrom.out.geneCovs
        results = analyse_chrom.out.chromResults
}
