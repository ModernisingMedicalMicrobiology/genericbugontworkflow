include {map} from '../modules/mapping.nf'                                     
include {pysamStats} from '../modules/mapping.nf'                              
include {clair} from '../modules/mapping.nf'                                   
include {makeGenomeClairConensus} from '../modules/mapping.nf'                 
include {genomeDepth} from '../modules/mapping.nf'                             

// mapping                                                                      
workflow mapping {
    take:                                                                       
        fastqs                                                                  
        refinputs                                                               
        clairChannel                                                            
        modelbase                                                               
                                                                                
    main:                                                                       
        map(refinputs.combine(fastqs))                                          
                                                                                
        pysamStats(map.out.fullAligned)                                         
                                                                                
        clair(map.out.fullAligned2.combine(clairChannel))                       
                                                                                
        makeGenomeClairConensus(clair.out.clairVCFs.combine(pysamStats.out.ch, by:0).combine(modelbase) )
                                                                                
        genomeDepth(map.out.unSubfullAligned)

        emit:
        unmasked     = makeGenomeClairConensus.out.unmasked
        masked     = makeGenomeClairConensus.out.masked
        pysamstats   = pysamStats.out.ch
        genomeDepths = genomeDepth.out.genomeDepths
        bamfiles    = map.out.fullAligned2
}   
                                                                                
                                                                                
