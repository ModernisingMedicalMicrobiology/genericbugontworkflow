// include modules                                                              
include {refVariants} from '../modules/analysis.nf'                          
include {indel_class} from '../modules/analysis.nf'                         
include {analyse_chrom} from '../modules/analysis.nf'                          
include {analyse_mobile} from '../modules/analysis.nf'                         
include {analyse_plasmid} from '../modules/analysis.nf'                         
include {report} from '../modules/analysis.nf'                         
include {mutation_report} from '../modules/analysis.nf'                         
    
// analysis                                                                     
workflow analysis{                                                              
    take:                                                                       
        masked                              
        pysamstats                                                              
        geneBed                                                                 
        mutations                                                               
        chromosome_determinants                                                 
        Accdepth                                                                
        mobile_determinants
        genomeDepths 
        bamfiles
        plasmidBlast
        geneBlast  
                                                                                
    main:
        // ## analysis of chromosome determinants from fasta and bam files                                                                       
        refVariants(masked.combine(pysamstats, by:0).combine(geneBed).combine(mutations))

        indel_class(bamfiles.combine(mutations).combine(geneBed))

        // ## combine into a single file
        refVariants.out.allrefVariants.combine(indel_class.out.indels_csv, by:0)
                    .collectFile(name:'chrom_variants.csv', 
                    sort: true, newLine: false, keepHeader:true)
                    .map{ file -> tuple(file.baseName, file) }
                    .set{ chrom_collected }
                                                                                
        analyse_chrom(chrom_collected
                    .combine(refVariants.out.geneCovs, by:0)
                    .combine(chromosome_determinants))


        // ## analysis of mobile determinants
        analyse_plasmid(plasmidBlast.combine(geneBlast,by:0))

        analyse_mobile(Accdepth
            .combine(mobile_determinants)
            .combine(genomeDepths, by:0)
            .combine(analyse_plasmid.out.csv, by:0))                   


        // ## combined report for chrom and mobile
        report(analyse_mobile.out.SR.combine(analyse_chrom.out.SR, by:0).combine(genomeDepths, by:0))

        report.out.tsv.collectFile(name:'sensitivity_report.tsv',
            storeDir:"report" ,
            keepHeader:true,
            skip:1) 

        mutation_report(analyse_mobile.out.mobileResults.combine(analyse_chrom.out.chromResults, by:0)
                    .combine(chromosome_determinants)         
                    .combine(mobile_determinants) )         
                                                                              
        mutation_report.out.tsv.collectFile(name:'gene_mutation_report.tsv',
            storeDir:"report" ,
            keepHeader:true,
            skip:1) 
}
