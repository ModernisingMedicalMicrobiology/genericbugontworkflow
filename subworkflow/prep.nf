include {classify} from '../modules/prep.nf'                                     
include {binReads} from '../modules/prep.nf'         
include {download_centrifuge} from '../modules/prep.nf'         
include {centrifuge_report} from '../modules/prep.nf'         

// prep workflow                                             
workflow prep{                                                                  
    take:                                                                       
        runs                                                                    
        centdb1                                                                 
    main:
        if (params.centdb == '') { 
        download_centrifuge()

        download_centrifuge.out.db
                           .map{row -> tuple(row[0].simpleName, row[1], row[1])}
                           .set{centdb1}
        }
                                                                      
        classify(runs.combine(centdb1))                                         
                
        binReads(classify.out.centFiles.combine(runs,by:0)) 

        centrifuge_report(classify.out.centFiles.combine(centdb1))
                                                               
    emit:
        fastqs = binReads.out.fastqs                                           
        cent = classify.out.centFiles                                           
}                                                      
