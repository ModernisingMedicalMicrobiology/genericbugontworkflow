# Resistance catalogues

The workflow needs a number of files to define the resistance genes and mutations. These can be added to the command line when running the workflow or added as a prarams profile to be imported such as `config/staph_aureus.config`.

## Files
These are the files required.

### Reference file
`ref` A fasta reference file, see `refs/MRSA252.fasta for an example`

### Genes bed
`geneBed` A bed file with the gene coordinates of interest, see `beds/MRSA252.bed`

### Mutations tab file
`mutations` A tab seperated file of gene name and amino acid mutations in WT-AA-Mut format e.g. `S84L`, and the presence/absence of the mutationion. Presence being the mutations confers resistance, absence is the opposite. This is to enable reference genomes with resistance INDEL already present. INDELs are currently limited to 1bp. See `beds/staph_mutations.tab`

### Chromosome determinants
`chromosome_determinants` A tab seperated file of genes, mutations and the antibiotics they resist, see `catalogs/staph_catalog/Staph_chromosome_determinants.tsv`.

| Antimicrobial agent | Gene | Amino acid substitutions                                                                                                                                                                                                                                                                                                                                                                                                                   | Reference gene accession no. (nucleotide positions) |
| ------------------- | ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------- |
| Ciprofloxacin       | gyrA | S84L, E88K, G106D, S85P, E88G, E88L                                                                                                                                                                                                                                                                                                                                                                                                        | BX571857.1 (7005–9668)                              |
| Ciprofloxacin       | grlA | S80F, S80Y, E84K, E84G, E84V, D432G, Y83N, A116E, A48T, D79V, V41G, S108N                                                                                                                                                                                                                                                                                                                                                                  | BX571857.1 (1386869–1389271)                        |
| Ciprofloxacin       | grlB | R470D*, E422D*, P451S*, P585S*, D443E*, R444S*                                                                                                                                                                                                                                                                                                                                                                                             | BX571857.1 (1384872–1386869)                        |
| Ciprofloxacin       | fusA | A160V*, A376V, A655E, A655P*, A655V*, A67T*, A70V*, A71V*, B434N, C473S*, D189G*, D189V*, D373N*, D463G*, E233Q*, E444K, E444V*, E449K*, F441Y, F652S*, G451V, G452C, G452S, G556S, G617D, G664S, H438N, H457Q, H457Y, L430S*, L456F, L461K, L461S, M161I*, M453I, M651I, P114H, P404L, P404Q, P406L, P478S, Q115L, R464C, R464H, R464S, R659C, R659H, R659L, R659S, R76C*, S416F*, T385N, T387I*, T436I, T656K, V607I, V90A, V90I, Y654N* | BX571857.1 (577685–579766)                          |
| Rifampin            | rpoB | A473T*, A477D, A477T*, A477V, D471G*, D471Y, D550G, H481D, H481N, H481Y, I527F, I527L*, I527 M*, ins 475H, ins G475*, L466S*, M470T*, N474K*, Q456K, Q468K, Q468L, Q468R, Q565R*, R484H, S463P, S464P, S486L, S529L*                                                                                                                                                                                                                       | BX571857 (568813–572364)                            |
| Trimethoprim        | dfrB | F99Y, F99S, F99I, H31N, L41F, H150R, L21V*, N60I*                                                                                                                                                                                                                                                                                                                                                                                          | BX571857.1 (1464014–1464493)                        |

### mobile_determinants
`mobile_determinants` Similar to `chromosome_determinants`, but only the mobile acquired genes needed, see `catalogs/staph_catalog/Staph_mobile_determinants.tsv`.

 
| Antimicrobial agent(s)            | Gene      | Product                                                                  | Reference gene accession no. (nucleotide positions) |
| --------------------------------- | --------- | ------------------------------------------------------------------------ | --------------------------------------------------- |
| Penicillin                        | blaZ      | Class A beta-lactamase                                                   | BX571856.1 (1913827-1914672)                        |
| Methicillin                       | mecA      | Low-affinity PBP2                                                        | BX571856.1 (44919-46925)                            |
| Erythromycin                      | msrA*     | Erythromycin resistance protein                                          | CP003194 (54168-55634)                              |
| Erythromycin and clindamycin      | ermA      | rRNA adenine N-6-methyltransferase                                       | BA000018.3 (56002-56733)                            |
| Erythromycin and clindamycin      | ermB      | rRNA adenine N-6-methyltransferase                                       | AB699882.1 (4971-5708)                              |
| Erythromycin and clindamycin      | ermC      | rRNA adenine N-6-methyltransferase                                       | HE579068 (7858-8592)                                |
| Erythromycin and clindamycin      | ermT      | 23S rRNA methylase                                                       | HF583292 (11344-12078)                              |
| Tetracycline                      | tetK      | MFS tetracycline effux pump                                              | FN433596 (69118-70497)                              |
| Tetracycline                      | tetL      | MFS tetracycline efflux pump                                             | HF583292 (7713-9089)                                |
| Tetracycline                      | tetM      | Ribosomal protection protein                                             | CP002643 (427033-428952)                            |
| Vancomycin                        | vanA      | Low-affinity peptidoglycan precursor                                     | AE017171.1                                          |
| Fusidic acid                      | fusB      | Fusidic acid detoxification                                              | CP003193.1 (1336-1977)                              |
| Fusidic acid                      | fusC      | Fusidic acid detoxification                                              | BX571857.1 (52820-53458)                            |
| Fusidic acid                      | far*      | Ribosome protection protein                                              | AY373761.1 (19072-19713)                            |
| Trimethoprim                      | dfrA      | Insensitive dihydrofolate reductase                                      | CP002120 (2093303-2093788)                          |
| Trimethoprim                      | dfrG      | Insensitive dihydrofolate reductase                                      | FN433596 (502263-502760)                            |
| Gentamicin                        | aacA-aphD | 6-aminoglycoside N-acetyltransferase/2-aminoglycoside phosphotransferase | FN433596.1 (2209531-2210970)                        |
| Mupirocin (high-level resistance) | mupA      | Isoleucyl-tRNA synthetase                                                | HE579068 (2157-5231)                                |
| Mupirocin (high-level resistance) | mupB      | Isoleucyl-tRNA synthetase                                                | JQ231224                                            |
| Erythromycin and clindamycin      | lsaE      |                                                                          | JX560992 (11387-12872)                              |
| Erythromycin and clindamycin      | vgaE      |                                                                          | FR772051 (8740-10315)                               |
| Erythromycin and clindamycin      | lnuA      |                                                                          | AM399080 (1664-2150)                                |
| Erythromycin and clindamycin      | ereA      |                                                                          | AY183453.1 (2730-3950)                              |
| Erythromycin and clindamycin      | ereB      |                                                                          | X03988.1 (383-1642)                                 |
| Erythromycin and clindamycin      | mefE      |                                                                          | AE007317.1 (383-1642)                               |
| Erythromycin and clindamycin      | mefB      |                                                                          | FJ196385.1 (11084-12313)                            |
| Erythromycin and clindamycin      | mefC      |                                                                          | AB571865.1 (144313-145536)                          |
| Erythromycin and clindamycin      | mefD      |                                                                          | MN728681.1 (17459-18658)                            |
| Erythromycin and clindamycin      | mphC      |                                                                          | AB013298.1 (2296-3195)                              |
| Erythromycin and clindamycin      | lnuB      |                                                                          | AJ238249.1 (127-930)                                |

### Plasmids

A fasta file with the sequences of expected plasmids. This is combined with `plasmid_genes` so only genes within contigs that match know plasmids are used to confer resistance.

### Plasmid_genes

A fasta file the sequences of genes that confer resistance and are found on plasmids, such as blaTEM-1 and tetM. 

## Command line
To add the required files on the command line, use commands such as these below to the overall `nextflow run` command constructed for your samples. 

```bash
--taxid 1280 \
--ref $base/refs/MRSA252.fasta \
-profile docker \
--geneBed $base/beds/MRSA252.bed \
--mutations $base/beds/staph_mutations.tab \
--genes ${base}/catalogs/staph_catalog/staph_genes.fasta \
--chromosome_determinants ${base}/catalogs/staph_catalog/Staph_chromosome_determinants.tsv \
--mobile_determinants ${base}/catalogs/staph_catalog/Staph_mobile_determinants.tsv
```
