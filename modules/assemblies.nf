process overlapAccGenes {    
    label 'notflye'                                                 
                                                   
    tag { run }                                                                 
    scratch true                                                                
    cpus 2                                                                      
                                                                                
    input:                                                                      
//    set val(r),file('ref'), val(run),file('*fq.gz'),val(crumpitname) from genes.combine(runs3)
    tuple val(r), path('ref'),val(run), path('fastq') 
                                                                                
    output:                                                                     
    tuple val(run), path('ref'), path('fastq'), path('overlaps.paf.gz'), emit: genePafs
                                                                                
    script:                                                                     
    """                                                                         
    minimap2 -t ${task.cpus} -x ava-ont \
          ref fastq | gzip > overlaps.paf.gz                                    
    """              

    stub:
    """
    touch overlaps.paf.gz
    """                                                           
}                                                                               
                                                                                
process binAccGenes {       
        label 'notflye'                                                 
                                                    
    tag { run }                                                                 
                                                                                
    input:                                                                      
    tuple val(run),file('ref'), path('fastq.gz'), path('overlaps.paf.gz'),path('binned_tax.fq.gz')
                                                                                
    output:                                                                     
    tuple val(run), path('ref'), path("trimmed.fastq"), emit: binnedAccGenes        
    tuple val(run), path('ref'), path("untrimmed.fastq"), emit: binnedUTAccGenes 
    tuple val(run), path("trimmed.fastq"), emit: binnedAccGenesNoRef 
                                                                                
    script:                                                                     
    """                                                                         
    binPafReads.py -p overlaps.paf.gz -f fastq.gz -ot trimmed.fastq -o untrimmed.fastq 
    """              

    stub:
    """
    touch trimmed.fastq
    touch untrimmed.fastq
    """                                                           
}                                                                               
                                                                                
process mapAccGenes {            
        label 'notflye'                                                 
                                               
    tag { run }                                                                 
    scratch true                                                                
    cpus=1                                                                      
                                                                                
    input:                                                                      
    tuple val(run), path('ref'), path("trimmed.fastq")         
                                                                                
    output:                                                                     
    tuple val(run), path('ref'), path('genes.sorted.bam'), path('genes.sorted.bam.bai'), emit: mapGenes
                                                                                
    script:                                                                     
    """                                                                         
    minimap2 -t ${task.cpus} -ax map-ont ref \
        trimmed.fastq | samtools view -F2052  -bS -o reads.bam -                
    samtools sort -@ ${task.cpus} reads.bam -o genes.sorted.bam                 
    rm reads.bam                                                                
    samtools index genes.sorted.bam                                             
    """             

    stub:
    """
    touch genes.sorted.bam
    touch genes.sorted.bam.bai
    """                                                            
}   

process accgenedepth {              
        label 'notflye'                                                 
                                            
  tag { run }                                                                   
//  scratch true                                                                
  publishDir 'accessoryGeneDepths'                                              
                                                                                
  input:                                                                        
  tuple val(run), path('ref'), path('genes.sorted.bam'), path('genes.sorted.bam.bai')
                                                                                
  output:                                                                       
  tuple val(run), path("${run}.csv"), emit: Accdepth                                 
                                                                                
  script:                                                                       
  """                                                                           
  samtools depth -aa genes.sorted.bam > ${run}_depth.tsv                        
  coverageStats.py ${run}_depth.tsv ${run}                                      
  mv coverage_stats.csv ${run}.csv                                              
  """             

  stub:
  """
  touch ${run}.csv
  """                                                              
}                                                                               
                                                                                
// process assembleAccessory {                                                     
//     tag { run }                                                                 
//     errorStrategy 'ignore'                                                      
//     cpus 4                                                                      
//     publishDir "assembledContigs/${task.process.replaceAll(":","_")}", overwrite: true, mode: 'copy'              
                                                                                
//     input:                                                                      
//     tuple val(run), path('ref'), path('untrimmed.fastq') 
                                                                                
//     output:                                                                     
//     tuple val(run), path("${run}.contigs.fa"), emit: contigs            
                                                                                
//     script:                                                                     
//     """                                                                         
//     wtdbg2 -t ${task.cpus} -i untrimmed.fastq -fo contigs -L 3000               
//     wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}.contigs.fa       
//     """          

//     stub:
//     """
//     touch ${run}.contigs.fa
//     """                                                               
// }   

process assembleAccessory { 
    label 'flye'                                                 
    tag { run }                                                                 
    errorStrategy 'ignore'      
    cpus 1                                                                    
    publishDir "assembledContigs/${task.process.replaceAll(":","_")}", overwrite: true, mode: 'copy'              
                                                                                
    input:                                                                      
    tuple val(run), path('ref'), path('untrimmed.fastq') 
                                                                                
    output:                                                                     
    tuple val(run), path("flye_assembly/assembly.fasta"), emit: contigs            
                                                                                
    script:                                                                     
    """  
    flye  --nano-raw untrimmed.fastq --out-dir flye_assembly || true                                                                     
    touch  touch flye_assembly/assembly.fasta                                                                          
    """          

    stub:
    """
    mkidir flye_assembly/
    touch flye_assembly/assembly.fasta
    """                                                               
}   

process makeblastdb {
        label 'notflye'                                                 

    tag {refname}
    input:
    tuple val(refname), path('ref')

    output:
    path('outf') 

    script:
    """
    mkdir outf
    cp $ref outf/ref.fa

    makeblastdb -in outf/ref.fa \
    -parse_seqids \
    -title ${refname} \
    -dbtype nucl
    """

    stub:
    """
    mkdir outf
    """
}


process blast {              
        label 'notflye'                                                 
                                          
    tag { run }                                                                 
                                                                                
    input:                                                                      
    tuple val(run), path("${run}.contigs.fa"), path('inf/')                                   
                                                                                
    output:                                                                     
    tuple val(run), file("${run}.O6.blast.txt"), emit: blast06
                                                                                
    script:                                                                     
    outfmt="6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"
    """                                                                         
    blastn -query ${run}.contigs.fa -db inf/outf/ref.fa -outfmt "$outfmt" -max_target_seqs 10000 > ${run}.O6.blast.txt
    """         

    stub:
    """
    touch ${run}.O6.blast.txt
    """                                                                
}                                   
