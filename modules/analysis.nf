// ################# chromosome analysis ########################

process refVariants {   
        label 'notflye'                                                 
                                                        
    tag { run }                                                                 
    publishDir 'refVariants', pattern: '*.csv', overwrite: true, mode: 'copy'   
                                                                                
    input:                                                                      
    tuple val(run), file("${run}_polishedGenome.fa"),file('ref.fa'),file('pysamstats.txt'),path('ref2.fa'),file('genes.bed'),file('mutations.tab') 
                                                                                
    output:                                                                     
    tuple val(run),file('*checkMuts.csv'), emit: allrefVariants
    tuple val(run),file('*geneCovs.csv'), emit: geneCovs
                                                                                
    script:                                                                     
    """                                                                         
    cntname=`(grep '>' ref.fa | cut -c 2-| cut -d ' ' -f1 )`                    
    checkMuts.py -s ${run}_polishedGenome.fa \
        -r ref.fa \
        -sn ${run} \
        -b genes.bed \
        -m mutations.tab \
        -rn \$cntname \
        -p pysamstats.txt                                                       
    """            

    stub:
    """
    touch ${run}_contig_checkMuts.csv
    touch ${run}_contig_geneCovs.csv
    """                                                             
}     

process indel_class {
        label 'notflye'                                                 

    errorStrategy 'ignore'
    tag { run }

    publishDir "refVariants/${task.process.replaceAll(":","_")}", mode: 'copy'
    
    input:
    tuple val(run), path('ref'), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai"),file('mutations.tab'),file('genes.bed')

    output:
    tuple val(run), path("${run}_indels.tab"),emit: indels_tab
    tuple val(run), path("${run}_indel_muts.csv"),emit: indels_csv

    script:
    """
    indel_class.py \
        -b ${run}.sorted.bam \
        -bed genes.bed \
        -muts mutations.tab \
        -s ${run}  \
        -o ${run}_indel_muts.csv \
        > ${run}_indels.tab
    """

    stub:
    """
    touch ${run}_indels.tab
    touch ${run}_indel_muts.csv
    """
}

process analyse_chrom {      
        label 'notflye'                                                 
                                                   
    tag { run }                                                                 
    errorStrategy 'ignore'                                                      
                                                                                
    publishDir 'AMRresults', pattern: '*_results.csv', mode: 'copy'             
    publishDir 'abx', pattern: '*_abx.csv', mode: 'copy'                        
                                                                                
    input:                                                                      
    tuple val(run),file('refVariants.csv'),file('geneCovs.csv'),file('chromosome_determinants.tsv')
                                                                                
    output:                                                                     
    tuple val(run),file("${run}_chromosome_results.csv"), emit: chromResults
    tuple val(run),file("${run}_chromosome_abx.csv"), emit: abx
    tuple val(run),file('geneCovs.csv'), emit: geneCovs
    tuple val(run),file("${run}_chromosome_SR.csv"),emit: SR
                                                                                
    script:                                                                     
    """                    
    analysis.py -s $run -i refVariants.csv -d chromosome_determinants.tsv -m chromosome -c geneCovs.csv
    """     

    stub:
    """
    touch ${run}_chromosome_results.csv
    touch ${run}_chromosome_abx.csv
    touch geneCovs.csv
    touch ${run}_chromosome_SR.csv
    """                                                                    
}                                                                               

// ################# mobile analysis ######################

process analyse_plasmid {
        label 'notflye'                                                 

    tag { run }
    publishDir 'accessoryResults', mode: 'copy'

    input:
    tuple val(run), path("${run}.O6.blast.txt"), path("${run}.genes.O6.blast.txt")

    output:
    tuple val(run), path("${run}_accessory_results.csv"), emit: details_csv
    tuple val(run), path("${run}_plasmid_coverages.csv"), emit: csv

    script:
    """
    detect_accessory.py -p ${run}.O6.blast.txt -b ${run}.genes.O6.blast.txt -s ${run}
    """

    stub:
    """
    touch ${run}_accessory_results.csv
    touch ${run}_plasmid_coverages.csv
    """

}

process analyse_mobile {       
        label 'notflye'                                                 
                                                 
    tag { run }                                                                 
    errorStrategy 'ignore'                                                      
    publishDir 'AMRresults', pattern: '*_results.csv', mode: 'copy'             
    publishDir 'abx', pattern: '*_abx.csv', mode: 'copy'                        
                                                                                
    input:                                                                      
    tuple val(run),file('coverages.csv'),file('mobile_determinants.tsv'), file('genome_covs.csv') 
                                                                                
    output:                                                                     
    tuple val(run),file("${run}_mobile_results.csv"), file("${run}_mobile_abx.csv"),emit: mobileResults
    tuple val(run),file("${run}_mobile_SR.csv"),emit: SR
                                                                                
    script:                                                                     
    """                                                                        
    analysis.py -s $run -i coverages.csv -g genome_covs.csv -d mobile_determinants.tsv -m mobile   
    """        

    stub:
    """
    touch ${run}_mobile_results.csv
    touch ${run}_mobile_abx.csv
    touch ${run}_mobile_SR.csv
    """                                                                 
}       


// ############## combine and report ######################


process report {
        label 'notflye'                                                 

    tag { run }                                                                 
    publishDir 'report', pattern: '*_report.tsv', mode: 'copy'             

    input:                                                                      
    tuple val(run), path('mobile_SR.csv'), path('chromosome_SR.csv'), file('genome_covs.csv')

    output:
    path("${run}_report.tsv"), emit: tsv

    script:
    """
    report.py -s ${run} \
        -m mobile_SR.csv \
        -c chromosome_SR.csv \
        -o ${run}_report.tsv \
        -d genome_covs.csv

    """

    stub:
    """
    touch ${run}_report.tsv
    """

} 

process mutation_report {
        label 'notflye'                                                 

    tag { run }                                                                 
    publishDir 'report', pattern: '*_mutation_report.tsv', mode: 'copy'             

    input:                                                                      
    tuple val(run), path('mobile_results.csv'), path('mobile_abx.csv'), 
        path('chromosome_results.csv'), file('chromosome_abx.csv'), file('geneCovs.csv'),
        file('chromosome_determinants.tsv'), file('mobile_determinants.tsv')

    output:
    path("${run}_mutation_report.tsv"), emit: tsv

    script:
    """
    mutation_report.py -s ${run} \
        -m mobile_results.csv \
        -c chromosome_results.csv \
        -o ${run}_mutation_report.tsv \
        -cd chromosome_determinants.tsv \
        -md mobile_determinants.tsv \
        -gc geneCovs.csv

    """

    stub:
    """
    touch ${run}_mutation_report.tsv
    """
}
