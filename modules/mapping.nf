process map {
        label 'notflye'                                                 

    tag { run + ' ' + r }                                                   
    errorStrategy 'ignore'                                                  
    cpus 3                   

    publishDir "bams/${task.process.replaceAll(":","_")}", overwrite: true                                           
                                                                            
    input:                                                                  
    tuple val(r), path('ref'), val(run), path('fastq')
                                                                            
    output:                                                                 
    tuple val(r),val(run), path('ref'), path(fastq), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai"), optional: true, emit: aligned
    tuple val(r),val(run), path('ref'), path("${run}.sortedfull.bam"), path("${run}.sortedfull.bam.bai"), emit: unSubfullAligned
    tuple val(r),val(run), path('ref'), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai"), optional: true, emit: fullAligned
    tuple val(run), path('ref'), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai"), optional: true, emit: fullAligned2
                                                                            
    script:
    """
    minimap2 -t 2 -ax map-ont ref 'fastq' | samtools view -q 50 -bS - | samtools sort -o ${run}.sortedfull.bam
    samtools index ${run}.sortedfull.bam                                    
    subSampleBam.py -b ${run}.sortedfull.bam -s 100 -o ${run}.sorted.bam    
    """ 

    stub:
    """
    touch ${run}.sorted.bam
    touch ${run}.sorted.bam.bai
    """
}


                                                                                
process pysamStats {      
        label 'notflye'                                                 
                                                      
    tag { run }                                                                 
                                                                                
    input:                                                                      
    tuple val(r),val(run),path('ref.fa'),path("${run}.sorted.bam"),path("${run}.sorted.bam.bai") 
                                                                                
    output:                                                                     
    tuple val(run), path("${run}_pysamfile.txt"), path('ref.fa'), emit: ch
                                                                                
    script:                                                                     
    """                                                                         
    pysamstats -t variation_strand ${run}.sorted.bam -f ref.fa > ${run}_pysamfile.txt
    """            

    stub:
    """
    touch ${run}_pysamfile.txt
    """                                                             
}                                                                               
                                                                                
                                                                                
process clair {            
        label 'notflye'                                                 
                                                     
    cpus 4                                                                      
    label 'clair'                                                               
    tag {run}                                                                   
                                                                                
    input:                                                                      
    tuple val(run), path('ref'), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai"), path('CLAIRMODEL') 
                                                                                
    output:                                                                     
    tuple val(run), path("${run}.vcf"), path('ref.fa'), emit: clairVCFs
                                                                                
    script:                                                                                                                          
    """                                                                         
    cat ref > ref.fa                                                         
    cntname=`(grep '>' ref.fa | cut -c 2-| cut -d ' ' -f1 )` 
    echo \$cntname                   
    samtools faidx ref.fa
    for cntg in \${cntname}
    do                                                       
    clair.py callVarBam \
        --chkpnt_fn CLAIRMODEL/model \
        --ref_fn ref.fa \
        --bam_fn ${run}.sorted.bam \
        --ctgName \$cntg \
        --sampleName ${run} \
        --minCoverage 1 \
        --threads ${task.cpus} \
        --call_fn ${run}_\${cntg}.vcf  
    done                        
    bcftools concat *.vcf > ${run}.vcf                          
    """                                       

    stub:
    """
    touch 'ref.fa'
    touch ${run}.vcf
    """                                  
}

process makeGenomeClairConensus {     
        label 'notflye'                                                 
                                          
    tag { run }                                                                 
                                                                                
    publishDir "clairConsensuSeqs/${task.process.replaceAll(":","_")}"
                                                                                
    input:                                                                      
    tuple val(run),file("${run}.vcf"), path('ref.fa'), path("${run}_pysamfile.txt"),path('ref2.fa'), path('modelbase') 
                                                                                
    output:                                                                     
    path("${run}.fasta.gz"), emit: genomeConsensusFiles                           
    path("input.txt"), emit: genomeInputFiles                                     
    tuple val("${run}"), path("${run}_polishedGenome.fa"), path('ref.fa'), emit: unmasked
    tuple val("${run}"), path("${run}_polishedGenome_masked.fa"),path('ref.fa'), emit: masked                      
                                                                                
    script:                                                                     
    md=params.maskdepth                                                         
    """ 
    filterVCF_model.py -i ${run}.vcf \
                -v clair \
                -o ${run}_genomefiltered.vcf \
                -m modelbase/clair_composite_model.sav  \
                -p ${run}_pysamfile.txt \
                -f 0                                                            
                                                                                
    nanopolish vcf2fasta --skip-checks -g ref.fa ${run}_genomefiltered.vcf > ${run}_polishedGenome.fa
                                                                                
    maskDepth.py -p ${run}_pysamfile.txt -f ${run}_polishedGenome.fa -o ${run}_polishedGenome_masked.fa -d ${md} -mw 0.8
                                                                                
    cat ${run}_polishedGenome_masked.fa | gzip > ${run}.fasta.gz                
    run=${run}                                                                  
    runShort=\${run:0:10}                                                       
    fp=\$(realpath ${run}.fasta.gz)                                             
    echo -e "\${runShort}\\t\${fp}" >> input.txt                                
    """          

    stub:
    """
    touch ${run}.fasta.gz
    touch input.txt
    touch ${run}_polishedGenome.fa
    touch ${run}_polishedGenome_masked.fa
    """                                                               
                                                                                
}                                                                               
                                                                                
process genomeDepth {         
        label 'notflye'                                                 
                                                  
    tag { run }                                                                 
    publishDir 'genomeDepths', pattern: '*.csv', overwrite: true, mode: 'copy' 
    publishDir 'genomeDepthsFull', pattern: '*.tsv', overwrite: true, mode: 'copy' 

    maxForks 1

    input:                                                                      
    tuple val(r),val(run), path('ref'), path("${run}.sorted.bam"), path("${run}.sorted.bam.bai")
                                                                                
    output:                                                                     
    tuple val(run), path("${run}_${r}_depth.csv"), emit: genomeDepths         
    tuple val(run), path("${run}_${r}_depth.tsv"), emit: tsv         

                                                                                
    script:                                                                     
    scriptBase=params.base + '/scripts'                                         
    """                                                                         
    samtools depth -aa ${run}.sorted.bam > ${run}_${r}_depth.tsv                
    coverageStats.py ${run}_${r}_depth.tsv ${run}                               
                                                                                
    mv coverage_stats.csv ${run}_${r}_depth.csv                                 
    """                 

    stub:
    """
    touch ${run}_${r}_depth.csv
    """                                                        
}                                                                               
                                                                                
