//################################# processes ################################//

process download_centrifuge {
        label 'notflye'                                                 

    output:
    tuple path("centdb/*.1.cf"), path('centdb'), emit: db

    script:
    centdb=params.dlCentrifugeindex
    """
    wget $centdb -O centdb.tar.gz
    tar -xvzf centdb.tar.gz
    mkdir centdb
    mv *.cf centdb
    """

    stub:
    """
    mkdir centdb
    touch centdb/centdb.{1..3}.cf
    """
}

process classify {
        label 'notflye'                                                 

    label 'classifiers'                       
    tag { run }                                                                 
    errorStrategy 'ignore'                                                      
                                                                                
    input:                                                                      
    tuple val(run),file('*fq.gz'), val(centdbName), file('centdbfol'), file('centdb')
                                                                                
    output:                                                                     
    tuple val(run), file("${run}.cent.tsv.gz"), emit: centFiles          
                                                                                
    script:                                                                     
    """                                                                     
    data=`ls -m *fq.gz | tr -d '\\n' | tr -d ' '`                           
    centrifuge -f -x centdbfol/${centdbName} \
       --mm -q -U \$data \
       -S ${run}.cent.tsv --min-hitlen 16 -k 1                              
    gzip ${run}.cent.tsv                                                    
    """                         

    stub:
    """
    touch "${run}.cent.tsv.gz"
    """                                            
}                                                                               
                                                                                
process binReads {              
        label 'notflye'                                                 
                                                
    errorStrategy 'ignore'                                                      
    tag { run }                  

    publishDir 'binned_reads'
                                                                                
    input:                                                                      
    tuple val(run), path("${run}.cent.tsv.gz"), path('*fq.gz') 
                                                                                
    output:                                                                     
    tuple val(run), path("bins/${run}_all_*"), optional: true, emit: fastqs
                                                                                
    script:                                                                     
    tax=params.taxid                                                            
    """                                                                         
    binReads.py -s ${run} \
        -fq *fq.gz \
        -cf ${run}.cent.tsv.gz \
        -tax ${tax} \
        -br all                                                                 
                                                                                
    """  

    stub:
    tax=params.taxid
    """
    mkdir bins
    touch bins/${run}_all_${tax}.fastq
    """                                                                       
                                                                                
}                         

process centrifuge_report {
        label 'notflye'                                                 

    label 'classifiers'                       
    tag { run }                                                                 
    errorStrategy 'ignore'                     
    publishDir 'centrifuge_reports', mode: 'copy'                                 

    input:
    tuple val(run), file("${run}.cent.tsv.gz"), val(centdbName), file('centdbfol'), file('centdb')

    output:
    tuple val(run), file("${run}_report.txt"), emit: reports

    script:
    """
    zcat ${run}.cent.tsv.gz | \
        centrifuge-kreport -x centdbfol/${centdbName} \
        > ${run}_report.txt
    """

    stub:
    """
    touch ${run}_report.txt
    """
}

