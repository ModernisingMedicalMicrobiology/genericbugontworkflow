# Test the workflow with some of our data

## Staphylococcus aureus

These instructions run two samples from our 2022 [paper](https://journals.asm.org/doi/10.1128/jcm.02156-21)

### Data
Download some test data with wget.

```bash
mkdir data && cd data
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR526/000/ERR5260720/ERR5260720.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR526/005/ERR5260555/ERR5260555.fastq.gz
cd ../
```

### prerequesites

[singularity](https://sylabs.io/guides/3.0/user-guide/installation.html) or [docker](https://docs.docker.com/engine/install/ubuntu/)

## RUN

```
nextflow run https://gitlab.com/ModernisingMedicalMicrobiology/genericbugontworkflow \
        --infastq data/*.fastq.gz \
	--species Staphylococcus_aureus \
        -profile singularity \
        -resume
```


## Results
This should output a file `report/sensitivity_report.tsv` with the following information in


| run name  | chrom      |    length |       bases |  avDepth | position cov1 | position cov10 | covBreadth1x | covBreadth10x | Ciprofloxacin | Erythromycin | Erythromycin and clindamycin | Fusidic acid | Gentamicin | Methicillin | Mupirocin (high-level resistance) | Penicillin | Rifampin | Tetracycline | Trimethoprim | Vancomycin |
| --------- | ---------- | --------- | ----------- | -------- | ------------- | -------------- | ------------ | ------------- | ------------- | ------------ | ---------------------------- | ------------ | ---------- | ----------- | --------------------------------- | ---------- | -------- | ------------ | ------------ | ---------- |
| ERR5260720 | BX571856.1 | 2,902,619 | 275,175,165 | 104.427… |     2,635,106 |      2,597,175 |       0.908… |        0.895… | S             | S            | S                            | R            | S          | S           | S                                 | S          | S        | R            | S            | S          |
| ERR5260555 | BX571856.1 | 2,902,619 | 272,412,882 |  99.647… |     2,733,769 |      2,708,790 |       0.942… |        0.933… | R             | S            | R                            | S            | S          | R           | S                                 | S          | S        | R            | R            | S          |
