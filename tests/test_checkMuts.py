#!/usr/bin/env python3
import pytest
from bin.checkMuts import checkMuts
import os

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
geneBedFile = os.path.join(data_dir, 'geneBedExample.bed')
mutsFile = os.path.join(data_dir, 'mutsExample.tab')
seqFile = os.path.join(data_dir, 'seqExample.fa')
refFile = os.path.join(data_dir, 'exampleRef.fa')
consensus1 = os.path.join(data_dir, 'WHOF_consensus.fasta')
psyamstatsFile = os.path.join(data_dir, 'WHOF_pysam.txt')



def test_loadGenes():
    cm=checkMuts(None,None,'testSample','testRef', geneBedFile,None, None)
    cm.loadGenes()
    chrom = cm.chrom_genes
    print(chrom)
    assert chrom['gyrA']['start']==1051396
    assert chrom['gyrA']['stop']==1054146
    assert chrom['gyrA']['score']==1
    assert chrom['gyrA']['strand']==1

def test_loadMuts():
    cm=checkMuts(None,None,'testSample','testRef', None,mutsFile, None)
    cm.loadMuts()
    muts=cm.mutations
    print(muts)
    assert muts['gyrA'] == ['S91F','D95N','D95G']
    assert muts['mtrR'] == ['G45D','A39T','ins -58A']

def test_loadSeq():
    cm=checkMuts(None,None,'testSample','testRef', None,None, None)
    chrom=cm.loadSeq(seqFile)
    print(chrom)
    assert str(chrom['test1']) == 'ACTG'
    assert str(chrom['test2']) == 'TGAC'

def test_runGenes():
    cm=checkMuts(consensus1,refFile,
    'testSample','testRef', geneBedFile,
    mutsFile, psyamstatsFile)

    cm.loadGenes()
    cm.loadMuts()
    cm.chrom=cm.loadSeq(cm.seq)
    cm.refs=cm.loadSeq(cm.refs)

    ## run main function to check genes
    cm.runGenes()
    print(cm.savemuts)
    print(cm.chrom_genes['gyrA']['prot'][0:10])
    assert str(cm.chrom_genes['gyrA']['prot'][0:10]) == 'MTDATIRHDH'

def test_geneCovs():
    cm=checkMuts(consensus1,refFile,
    'testSample','testRef', geneBedFile,
    mutsFile, psyamstatsFile)

    cm.loadGenes()
    cm.loadMuts()
    cm.chrom=cm.loadSeq(cm.seq)
    cm.refs=cm.loadSeq(cm.refs)

    ## run main function to check genes
    cm.runGenes()

    df=cm.geneCovs()
    print(df)
    #df=df.set_index(['chrom'])
    assert df.loc['gyrA','mean coverage'] == pytest.approx(72.198,1)
    assert df.loc['mtrR','mean coverage'] == pytest.approx(72.198,1)
