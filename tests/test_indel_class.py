#!/usr/bin/env python3
import pytest
import pandas as pd
from bin.indel_class import run_pos, getINDELs, getPOS, interpret
import os

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
WHOQBamFile = os.path.join(data_dir, 'WHOQBamFile.sorted.bam')
WHOFBamFile = os.path.join(data_dir, 'WHOFBamFile.sorted.bam')
noMTRSeqsBamFile = os.path.join(data_dir, 'noMTRSeqsBamFile.sorted.bam')
muts = os.path.join(data_dir, 'mutsExample.tab')
bed_file = os.path.join(data_dir, 'geneBedExample.bed')


def test_run_WHOF():
    df=pd.read_csv(muts,sep='\t')
    df=df.apply(getINDELs,axis=1)
    df=df[df['indel']==True] 
    bdf=pd.read_csv(bed_file,sep='\t')
    df=df.merge(bdf, on='gene', how='left')
    df=df.apply(getPOS,axis=1)
    s="sample name,gene,mut type,AA pos,WT res,res,EXP res\n"
    for i,r in df.iterrows():
        t=run_pos(WHOFBamFile, r['chrom'], r['pos'], 'ins_Strain')
        assert t[0] == pytest.approx(38.88,0.1)
        mut_type,res=interpret(t, r['WT res'], r['EXP res'], r['presence/absence'])
        s+=f"ins_Strain,{r['gene']},{mut_type},{r['mut pos']},{r['WT res']},{res},{r['EXP res']}\n"
    #print(s)
    assert mut_type == 'Wild type'

def test_run_WHOQ():
    df=pd.read_csv(muts,sep='\t')
    df=df.apply(getINDELs,axis=1)
    df=df[df['indel']==True] 
    bdf=pd.read_csv(bed_file,sep='\t')
    df=df.merge(bdf, on='gene', how='left')
    df=df.apply(getPOS,axis=1)
    s="sample name,gene,mut type,AA pos,WT res,res,EXP res\n"
    for i,r in df.iterrows():
        t=run_pos(WHOQBamFile, r['chrom'], r['pos'], 'ins_Strain')
        assert t[0] < 30
        assert t[1] < 30
        assert t[2] < 30
        assert t[3] < 30
        mut_type,res=interpret(t, r['WT res'], r['EXP res'], r['presence/absence'])
        s+=f"ins_Strain,{r['gene']},{mut_type},{r['mut pos']},{r['WT res']},{res},{r['EXP res']}\n"
    print(s)
    assert mut_type == 'Mutation'

def test_run_noSeqs():
    df=pd.read_csv(muts,sep='\t')
    df=df.apply(getINDELs,axis=1)
    df=df[df['indel']==True] 
    bdf=pd.read_csv(bed_file,sep='\t')
    df=df.merge(bdf, on='gene', how='left')
    df=df.apply(getPOS,axis=1)
    s="sample name,gene,mut type,AA pos,WT res,res,EXP res\n"
    for i,r in df.iterrows():
        t=run_pos(noMTRSeqsBamFile, r['chrom'], r['pos'], 'ins_Strain')
        assert t==None