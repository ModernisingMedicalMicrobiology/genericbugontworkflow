#!/usr/bin/env python3
from doctest import Example
import pytest
from bin.report import report
import os

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')

exampleMobileSR = os.path.join(data_dir, 'exampleMobileSR.csv')
exampleChromSR = os.path.join(data_dir, 'exampleChromSR.csv')
genomeDepths = os.path.join(data_dir, 'genome_covs.csv')


def test_overWriteSens():
    r=report('test1',None, None,
        None, None)
    allS=['S','S','S','S']
    allR=['R','R','R','R']
    oneR=['R','S','S','S']
    oneS=['R','R','R','S']
    allU=['U','U','U','U']
    RandU=['R','U','U','U']
    SandU=['S','U','U','U']
    RSU=['S','R','U','U']

    assert r.overWriteSens(allS) == 'S'
    assert r.overWriteSens(allR) == 'R'
    assert r.overWriteSens(oneR) == 'R'
    assert r.overWriteSens(oneS) == 'R'
    assert r.overWriteSens(allU) == 'U'
    assert r.overWriteSens(RandU) == 'R'
    assert r.overWriteSens(SandU) == 'S'
    assert r.overWriteSens(RSU) == 'R'



#def test_combineResults():
#    r=report('test1',exampleMobileSR, exampleChromSR,
#        genomeDepths, None)
#    pass