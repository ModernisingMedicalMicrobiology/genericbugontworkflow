#!/usr/bin/env python3
import pytest
from bin.analysis import analysis
import os

modules_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(modules_dir, 'data')
determinantsFile = os.path.join(data_dir, 'determinantsFile.tsv')
mobiledeterminantsFile = os.path.join(data_dir, 'mobile_determinants.tsv')
results_sens = os.path.join(data_dir, 'WHOF_refVariants.csv')
results_res = os.path.join(data_dir, 'WHOQ_refVariants.csv')
results_res2 = os.path.join(data_dir, 'WHOQ_refVariants2.csv')
geneCovs_sens = os.path.join(data_dir, 'WHOF_geneCovs.csv')
geneCovs_res = os.path.join(data_dir, 'WHOQ_geneCovs.csv')

mobile_results_sens = os.path.join(data_dir, 'WHO_mobile_coverages.csv')
mobile_genomeCovs_sens = os.path.join(data_dir, 'WHO_genome_covs.csv')

#mobile_results_res = os.path.join(data_dir, 'WHOQ_mobile_coverages.csv')
#mobile_genomeCovs_res = os.path.join(data_dir, 'WHOQ_genome_covs.csv')

def test_loadDeterminants():
    a=analysis('test1', determinantsFile, None, 'chromosome')
    a.loadDeterminants()
    print(a.df)

    assert len(a.df) == 12

def test_loadResults():
    a=analysis('test1', None, results_sens, 'chromosome')
    a.loadResults()
    print(a.results)

    assert len(a.results) == 19

def test_loadGeneCovs():
    a=analysis('test1', None, None, 'chromosome', geneCovs=geneCovs_sens)
    a.loadGeneCovs()

    print(a.geneCovsDF)
    assert len(a.geneCovsDF) == 15

def test_predictRow():

    a=analysis('test1', determinantsFile, 
    results_sens, 'chromosome', geneCovs=geneCovs_sens)
    
    row={'mut type':'Mutation',
        'minimun coverage':100,
        'mean coverage':100}
    row=a.predictRow(row)
    assert row['SR genotype'] == 'R'

    row={'mut type':'Wild type',
        'minimun coverage':100,
        'mean coverage':100}
    row=a.predictRow(row)
    assert row['SR genotype'] == 'S'

    row={'mut type':'Mutation',
        'minimun coverage':5,
        'mean coverage':5}
    row=a.predictRow(row)
    assert row['SR genotype'] == 'U'

def test_compare_sens():
    a=analysis('test1', determinantsFile, results_sens, 'chromosome', geneCovs=geneCovs_sens)
    a.loadDeterminants()
    a.loadResults()
    a.loadGeneCovs()
    a.compare()
    print(a.abx)
    print(a.results)

    assert len(a.results) == 0
    assert len(a.abx) == 0

def test_predictSR_sens():
    a=analysis('test1', determinantsFile, results_sens, 'chromosome', geneCovs=geneCovs_sens)
    a.loadDeterminants()
    a.loadResults()
    a.loadGeneCovs()
    a.compare()
    a.predictSR()

    print(a.sr)
    predicts=a.sr['SR genotype'].to_list()
    print(predicts)
    assert predicts == ['S','S','S','S','S','S','U','S','S','S']

def test_compare_res():
    a=analysis('test1', determinantsFile, results_res, 'chromosome', geneCovs=geneCovs_res)
    a.loadDeterminants()
    a.loadResults()
    a.loadGeneCovs()
    a.compare()
    print(a.abx)
    print(a.results.columns)
    print(a.results[['gene','mut type','Antimicrobial agent', 'Amino acid substitutions']])

    assert len(a.abx) == 8
    assert len(a.results) == 8

def test_predictSR_res():
    a=analysis('test1', determinantsFile, results_res, 'chromosome', geneCovs=geneCovs_res)
    a.loadDeterminants()
    a.loadResults()
    a.loadGeneCovs()
    a.compare()
    a.predictSR()

    print(a.sr)
    predicts=a.sr['SR genotype'].to_list()
    assert predicts == ['R','R','S','S','R','R','R','R','R','R']

def test_compareMobile_sens():
    a=analysis('test1', mobiledeterminantsFile, mobile_results_sens, 
    'mobile', geneCovs=mobile_genomeCovs_sens)
    a.loadDeterminants()
    a.loadResults()
    a.compareMobile()
    print(a.abx)
    print(a.results)

    assert len(a.results) == 1
    assert len(a.abx) == 1

#def test_compareMobile_res():
#    a=analysis('test1', mobiledeterminantsFile, mobile_results_res, 
#    'mobile', geneCovs=mobile_genomeCovs_res)
#    a.loadDeterminants()
#    a.loadResults()
#    a.compareMobile()
#    print(a.abx)
#    print(a.results)
#
#    assert len(a.results) == 0
#    assert len(a.abx) == 0