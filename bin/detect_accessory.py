#!/usr/bin/env python3
import pandas as pd
from argparse import ArgumentParser
import sys

headers=['qseqid', 'sseqid', 'pident', 'length', 'mismatch',\
            'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore',
            'qlen', 'slen']

def setIntervals(r):
    if r['qstart'] < r['qend']:
        return pd.Interval(r['qstart'], r['qend'],closed='both')
    else:
        return pd.Interval(r['qend'], r['qstart'], closed='both')

def loadBlast(blast,s):
    df=pd.read_csv(blast, sep='\t', names=headers)
    if len(df) == 0:
        df.to_csv('{0}_accessory_results.csv'.format(s),index=False)
        print('no blast results, exiting')
        header='run name,chrom,length,bases,avDepth,position cov1,position cov10,covBreadth1x,covBreadth10x'
        with open('{0}_plasmid_coverages.csv'.format(s),'wt') as outf:
            outf.write(header)
        sys.exit()
    df['file']=blast
    #df['Query prop']=df['length']/df['qlen']
    df['Sub prop']=df['length']/df['slen']
    df['q inv']=df.apply(setIntervals, axis=1)
    df['overlaps']=0
    df['overlap lengths']=0
    for i,row in df.iterrows():
        for j,rrow in df.iterrows():
            if list(row[['qseqid', 'sseqid']]) != list(rrow[['qseqid', 'sseqid']]): continue
            if i == j: continue
            if row['q inv'].overlaps(rrow['q inv']):
                df.loc[i,'overlaps']+=1
                l=range(max(row['qstart'], rrow['qstart']), min(row['qend'], rrow['qend'])+1)
                df.loc[i,'overlap lengths']=l[-1]-l[0]
    return df

def loadGene(blast):
    df=pd.read_csv(blast, sep='\t', names=headers)
    df['file']=blast
    df['Sub prop']=df['length']/df['slen']
    return df

def detect_accessory(p,g,s):
    df=loadBlast(p,s)
    df['total length']=df.groupby(['qseqid', 'sseqid'])['length'].transform(sum)
    df['overlaps length']=df.groupby(['qseqid', 'sseqid'])['overlap lengths'].transform(sum)
    df['actual length']=df['total length']-df['overlaps length']
    df['Query prop']=df['actual length']/(df['qlen'])
    df=df[['qseqid', 'sseqid','Query prop']]
    df.drop_duplicates(inplace=True)
    df2=loadGene(g)
    df=df.merge(df2,left_on=['qseqid'], right_on=['qseqid'], how='left')
    df['Sample name']=s
    print(df)

    df=df[['Sample name','qseqid', 'sseqid_x','sseqid_y', 'Query prop', 'pident', 'Sub prop', 'qlen', 'slen']]
    df=df[df['Query prop']>0.60]
    df=df[df['pident']>95]
    df.drop_duplicates(inplace=True)
    print(df)
    df.to_csv('{0}_accessory_results.csv'.format(s),index=False)

    # for mobile analysis
    df['run name']=s
    df['chrom']=df['sseqid_y']
    df['length']=df['slen']
    df['bases']=df['slen']
    df['avDepth']=20
    df['position cov1']=df['slen']
    df['position cov10']=df['slen']
    df['covBreadth1x']=df['Sub prop']
    df['covBreadth10x']=df['Sub prop']
    df=df[['run name',
            'chrom','length','bases',
            'avDepth','position cov1','position cov10',
            'covBreadth1x','covBreadth10x']]
    df.to_csv('{0}_plasmid_coverages.csv'.format(s),index=False)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Filter blast file to determine accessory identity')
    parser.add_argument('-p', '--plasmid_blast', required=True,
                             help='blast file of plasmids')
    parser.add_argument('-b', '--gene_blast', required=True,
                             help='blast file of genes')
    parser.add_argument('-s', '--sample_name', required=False,default=None,
                             help='Sample name')
    opts, unknown_args = parser.parse_known_args()

    bf=detect_accessory(opts.plasmid_blast,
            opts.gene_blast,
            opts.sample_name)

