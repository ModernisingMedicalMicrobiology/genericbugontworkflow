#!/usr/bin/env python3
import pandas as pd
import pysam
import sys
import os
from argparse import ArgumentParser, SUPPRESS

def getDepth(bam):
    names=['chrom','pos','depth']
    #df=pd.read_csv(depth, sep='\t',names=names)
    d=pysam.depth(bam)
    d=d.split('\n')
    d=[i.split('\t') for i in d]
    print(len(d[0]))
    if len(d[0])==1:
        print('No mapped reads! Exiting gracefully')
        sys.exit()
    df=pd.DataFrame(d,columns=['chrom','pos','depth'])
    median=df.depth.median()
    return float(median)

def subSample(bam, ratio, depth, outBam):
    if outBam==None:
        basename=os.path.basename(bam)
        subBam=basename.split('.')[0]
        subBam=subBam + '.depth' + str(depth) + '.bam'
    else:
        subBam=outBam
    print(subBam)
    fh = open(subBam, 'w')
    fh.close()
    pysam.view("-b", "-s", "{0}".format(str(ratio)),
		 "{0}".format(bam),"-o", "{0}".format(subBam), save_stdout=subBam)
    pysam.index("{0}".format(subBam))

def run(opts):
    me=getDepth(opts.bam_file)
    print(me, opts.sub_sample)
    for sub in opts.sub_sample:
        r=float(sub)/me
        if r>1:
            print('Cannot subsample {0}, not enough reads for {0}x depth'.format(sub,me))
            if opts.out_file!=None:
                os.symlink(opts.bam_file, opts.out_file)
                pysam.index("{0}".format(opts.out_file))
            continue
        subSample(opts.bam_file,r,sub,opts.out_file)
    

if __name__ == '__main__':
    parser = ArgumentParser(description='sub sample wrapper using samtools/pysam')
    parser.add_argument('-b', '--bam_file', required=True,
                        help='bam file')
    parser.add_argument('-s', '--sub_sample', required=True,nargs='+',
                        help='sub sample target depth(s)')

    parser.add_argument('-o', '--out_file', required=False,default=None,
                        help='output bam file, can only be used with one subsample')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
