#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
import pysam

class binfastq:
    def __init__(self, bam, fastqs, outfastq):
        self.bam = bam
        self.fastqs=fastqs
        self.outfastq=outfastq

    def getNames(self):
        samfile = pysam.AlignmentFile(self.bam, "rb")
        self.reads=set()
        for read in samfile.fetch():
            self.reads.add(read.query_name)

    def _getSeqs(self):
        for fastq in self.fastqs:
            for seq in SeqIO.parse(open(fastq, 'rt'),'fastq'):
                if seq.id in self.reads:
                    yield seq

    def makeFastqs(self):
        _seqs=self._getSeqs()
        SeqIO.write(_seqs, self.outfastq, 'fastq')

    def run(self):
        self.getNames()
        self.makeFastqs()

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='bam file to fastq file, using original fastq files so no trimming')
    parser.add_argument('-b', '--bam', required=True,
                             help='bam file')
    parser.add_argument('-f', '--fastq', required=True, nargs='+',
                             help='fastq files(s)')
    parser.add_argument('-o', '--outfastq', required=True,
                             help='output fastq file')
    opts, unknown_args = parser.parse_known_args()
    
    bf=binfastq(opts.bam, opts.fastq, opts.outfastq)
    bf.run()
            

        
