#!/usr/bin/env python3
import pandas as pd
import sys
import os
from argparse import ArgumentParser


def run(opts):
    dfs=[]
    for csv in opts.csvs:
        df=pd.read_csv(csv)
        dfs.append(df)
    df=pd.concat(dfs)
    df.drop_duplicates(inplace=True)
    df['genotype']='R'
    df2=pd.pivot_table(df, values='genotype',index='sample name',columns='Antimicrobial agent',
            aggfunc=lambda x: ' '.join(x))
    print(df2)



def compareGetArgs(parser):
    parser.add_argument('-c','--csvs',required=True,nargs='+',
            help='csv file(s)')
    return parser


if __name__ == '__main__':
    parser = ArgumentParser(description='Check mutations from panel and a reference genome')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    run(opts)

