#!/usr/bin/env python3
import sys
from argparse import ArgumentParser
import pandas as pd
from pandas import Series

class report:
    def __init__(self, samplename, mobile, chrom, 
            chrom_determinants, mobile_determinants, 
            gene_covs, output):
        self.sampleName=samplename
        self.output=output
        self.mobile=self.loadFile(mobile)
        self.mobile.drop_duplicates(subset=['chrom'],inplace=True)
        self.chrom=self.loadFile(chrom)
        self.chrom_determinants=self.loadTSVFile(chrom_determinants)
        self.mobile_determinants=self.loadTSVFile(mobile_determinants)
        self.setColumns()
        self.gene_covs=self.loadFile(gene_covs)

        self.combineResults()
        self.saveReport()

    def setColumns(self):
        # put chromosome genes into order and rename
        genes=list(self.chrom_determinants['Gene'].unique())
        genes.sort()
        chromMutCols=['{} mutations'.format(gene) for gene in genes]
        chromCovCols=['{} mean coverage'.format(gene) for gene in genes]
        colMap=dict(zip(genes,chromMutCols))
        covMap=dict(zip(genes,chromCovCols))
        chromMutCols.sort()
        chromCovCols.sort()

        # put mobile gene into order and rename
        genes=list(self.mobile_determinants['Gene'].unique())
        mutCols=['{} cov breadth (10x)'.format(gene) for gene in genes]
        mutCols.sort()
        self.mutCols=mutCols

        # combine cols
        self.colOrder=['sample name']
        self.colOrder.extend(mutCols)
        self.colOrder.extend(chromCovCols)
        self.colOrder.extend(chromMutCols)

    def loadFile(self,f):
        df=pd.read_csv(f)
        df=df.drop_duplicates()
        return df

    def loadTSVFile(self,f):
        return pd.read_csv(f,sep='\t')

    def combineResults(self):
        ### manipulate mobile gene coverages
        mobile=self.mobile
        mobile['chrom']=mobile['chrom'] + ' cov breadth (10x)'
        addRows=[]
        for row in self.mutCols:
            if row not in mobile['chrom'].unique():
                addRows.append({'run name':self.sampleName,
                    'chrom':row,
                    'covBreadth1x':None})
        addRows=pd.DataFrame(addRows)
        mobile=pd.concat([mobile, addRows])
        mobile=mobile.pivot(index='run name',
                columns='chrom', values='covBreadth10x').reset_index()
        #mobile['run name']=str(self.sampleName)
        #for col in self.mutCols:
        #    if col not in mobile:
        #        mobile[col]=None

        # manipulate chromosome mutations
        self.chrom['mutation']=self.chrom['WT res'] + self.chrom['AA pos'].astype(str)  + self.chrom['res']
        self.chrom.drop_duplicates(subset=['sample name','gene','mutation'],inplace=True)
        self.chrom['mutations']=self.chrom.groupby(['sample name','gene'])['mutation'].transform(lambda x: ','.join(x))
        self.chrom=self.chrom[['sample name', 'gene', 'mutations']]
        self.chrom.drop_duplicates(inplace=True)
        chrom=self.chrom.pivot(index='sample name',
                columns='gene',values='mutations')


        # put chromosome genes into order and rename
        genes=list(self.chrom_determinants['Gene'].unique())
        genes.sort()
        for gene in genes:
            if gene not in chrom.columns:
                chrom[gene]=None

        newCols=['{} mutations'.format(gene) for gene in genes]
        colMap=dict(zip(genes,newCols))
        chrom.rename(columns=colMap,inplace=True)
        self.chrom['sample name']=self.sampleName
        
        # merge mobile covs and chromosome mutations
        self.report=mobile.merge(chrom, right_on='sample name',
                left_on='run name', how='left')

        self.gene_covs.reset_index()#.index.rename('gene', inplace=True)
        self.gene_covs['gene']=self.gene_covs['Unnamed: 0'] +' mean coverage'
        self.gene_covs=self.gene_covs[['sample name','gene','mean coverage']]
        gene_cov=self.gene_covs.pivot(index='sample name',
                columns='gene',values='mean coverage').reset_index()

        self.report=self.report.merge(gene_cov, right_on='sample name',
                left_on='run name',how='left')
        self.report.drop('sample name',axis=1, inplace=True)
        self.report.rename(columns={'run name':'sample name'},inplace=True)

        # order columns
        for col in self.colOrder:
            if col not in self.report:
                if col == 'sample name':
                    self.report[col]=self.sampleName
                else:
                    self.report[col]=None
        self.report=self.report[self.colOrder]
        print(self.report)

    def saveReport(self):
        self.report.to_csv(self.output,
                sep='\t',
                index=False)


def runAnalysis(opts):
    r=report(opts.sample,
        opts.mobile,
        opts.chrom,
        opts.chrom_determinants,
        opts.mobile_determinants,
        opts.gene_covs,
        opts.output)


def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=True,default='unknown sample',
            help='sample name')
    parser.add_argument('-m','--mobile',required=True,
            help='mobile SR analys')
    parser.add_argument('-c','--chrom',required=True,
            help='chromosome SR analysis')
    parser.add_argument('-cd','--chrom_determinants',required=True,
            help='chromosome determinants')
    parser.add_argument('-md','--mobile_determinants',required=True,
            help='mobile determinants')
    parser.add_argument('-gc','--gene_covs',required=True,
            help='chromosome gene coverage')
    parser.add_argument('-o','--output',required=True,
            help='output tsv file')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='report AMR genes and mutations from determinants file and results')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runAnalysis(opts)

