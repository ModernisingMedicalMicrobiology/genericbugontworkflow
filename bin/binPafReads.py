#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO
from Bio import Seq
from Bio import SeqRecord
import pandas as pd
import gzip
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def getDescendants(taxid):
    dnts = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
    dnts.append(taxid)
    dnts.extend(ncbi.get_lineage(taxid))
    dnts=list(map(int,dnts))
    return dnts

class binfastq:
    def __init__(self, paf, fastqs, outtrim, centfile, taxids, outf=False):
        self.paf = paf
        self.fastqs=fastqs
        self.outtrimfastq=outtrim
        self.outfastq=outf
        self.centfile=centfile
        self.taxids=taxids

    def getAro(self,s):
        aro=s.split('|')[4].replace('ARO:','')
        return aro

    def getNames(self):
        names=['read id','read length','read start', 'read end', 'strand', 
		'gene name', 'gene length', 'gene start', 'gene end', 
		'res matches', 'alignmnent length', 'qual']
        uscols=list(range(0,len(names),1))
        self.df=pd.read_csv(self.paf,sep='\t',names=names,compression='gzip',usecols=uscols)
        self.df.sort_values(by=['alignmnent length'], ascending=False, inplace=True)
        self.df.drop_duplicates(subset=['read id'], keep='first', inplace=True)
#        self.df=self.df[self.df['gene length']<50000]
#        self.df=self.df[self.df['gene name']=='gb|KC243783|+|0-2007|ARO:3000617|mecA']

    def getReads(self):
        df=pd.read_csv(self.centfile,sep='\t')
        taxids=[]
        for tax in self.taxids:
            taxids.extend(getDescendants(tax))
        taxids.extend([1,0])
        df['taxID']=df['taxID'].map(int)
        df2=df[df['taxID'].isin(taxids)]
        df3=df[df['seqID'].isin(['phylum','order','class','superkingdom'])]
        self.readIds=list(df2['readID'].unique())
        self.readIds.extend(df3['readID'].unique())

    def getSeqs(self,trim=True):
        self.keepseqs={}
#        reads=list(set(self.df['read id'].unique()) & set(self.readIds))
        reads=set(self.df['read id'].unique())
        for fastq in self.fastqs:
            if fastq.endswith('.gz'):
                mo=gzip.open
            else:
                mo=open
            for seq in SeqIO.parse(mo(fastq, 'rt'),'fastq'):
                if seq.id in reads:
                    if trim==False:
                        yield seq
                        continue

                    df=self.df[self.df['read id']==seq.id]
                    n=0
                    for index, row in df.iterrows():
                        if int(row['read start']) - int(row['read end']) < 0:
                            ss=str(seq.seq[int(row['read start']):int(row['read end'])])
                            quals=seq.letter_annotations['phred_quality'][int(row['read start']):int(row['read end'])]
                        else:
                            ss=str(seq.seq[int(row['read end']):int(row['read start'])])
                            quals=seq.letter_annotations['phred_quality'][int(row['read end']):int(row['read start'])]
                        #seq.letter_annotations={}

                        rec = SeqRecord.SeqRecord(id=str(seq.id), description='', seq=Seq.Seq(ss), 
				letter_annotations={'phred_quality':quals})
                        #seq.seq=ss
                        #seq.letter_annotations={'phred_quality':quals}
                        if n>0:
                            rec.id=seq.id+'_'+str(n)
#                            seq.id=seq.id+'_'+str(n)
                        yield seq 
                        n+=1
		
    def maketrimmedFastqs(self):
        with open(self.outtrimfastq, 'wt') as outf:
            SeqIO.write(self._seqs, outf, 'fastq')

    def makeFastqs(self):
        with open(self.outfastq, 'wt') as outf:
            SeqIO.write(self._seqs, outf, 'fastq')

    def run(self):
        self.getNames()
        #self.getReads()
        self._seqs=self.getSeqs()
        self.maketrimmedFastqs()
        if self.outfastq != False:
            self._seqs=self.getSeqs(trim=False)
            self.makeFastqs()


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='paf file to fastq file, using original fastq files and trimming based on overlaps in paf file')
    parser.add_argument('-p', '--paf', required=True,
                             help='bam file')
    parser.add_argument('-f', '--fastq', required=True, nargs='+',
                             help='fastq files(s)')
    parser.add_argument('-ot', '--outtrimmed', required=True,
                             help='output trimmed fastq')
    parser.add_argument('-o', '--outf', required=False,
                             help='output fastq')
    parser.add_argument('-c', '--centFile', required=False,
                             help='centrifuge output file')
    parser.add_argument('-t', '--taxid', required=False,nargs='+',
                             help='taxids to include as well as unclassified')
    opts, unknown_args = parser.parse_known_args()

    bf=binfastq(opts.paf, 
            opts.fastq, 
            opts.outtrimmed,
            opts.centFile,
            opts.taxid,
            outf=opts.outf)
    bf.run()

