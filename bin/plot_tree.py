#!/usr/bin/env python3
from ete3 import PhyloTree,NodeStyle, TreeStyle
import sys
from argparse import ArgumentParser

def run(opts):
    if opts.names != None:
        names=open(opts.names,'rt').read()
        names=names.split('\n')
    else:
        names=[]
    treeFile=open(opts.inTree,'rt').read()
    t = PhyloTree(treeFile,format=1)

    ts = TreeStyle()
    ts.show_leaf_name = True

    # Draws nodes as small red spheres of diameter equal to 10 pixels
    nstyle = NodeStyle()
    nstyle['bgcolor'] = 'LightGrey'

    # Applies the same static style to all nodes in the tree. Note that,
    # if "nstyle" is modified, changes will affect to all nodes
    for name in names:
        name=name.replace('\n','')
        print(name)
        nodes=t.get_leaves_by_name(name) 
        for n in nodes:
            n.set_style(nstyle)

    print(t)
    t.render('{0}.svg'.format(opts.outTree))
    t.render('{0}.png'.format(opts.outTree),w=183, units="mm")

if __name__ == "__main__":
    parser = ArgumentParser(description='plot tree')
    parser.add_argument('-t', '--inTree', required=True,
                                 help='Input tree newick file')
    parser.add_argument('-o', '--outTree', required=True,
                                 help='Output tree png and svg files')
    parser.add_argument('-n', '--names',required=False,
                                 help='names to highlight')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
    

