#!/usr/bin/env python3
from Bio import SeqIO
import sys
import json
from argparse import ArgumentParser
import pandas as pd

class checkMuts:
    def __init__(self,seq,refs,samplename,refname,geneBed,muts,pysamstatsfile):
        """Set variable names, lists and dicts."""
        self.chrom={}
        self.savemuts=[]
        self.refname=refname
        self.samplename=samplename
        self.geneBed=geneBed
        self.muts=muts
        self.psf=pysamstatsfile
        self.seq=seq
        self.refs=refs

    def run(self):
        """Run methods and produce results."""
        self.loadGenes()
        self.loadMuts()
        self.chrom=self.loadSeq(self.seq)
        self.refs=self.loadSeq(self.refs)

        ## run main function to check genes
        self.runGenes()

        ## finish
        self.writeMuts()
        self.geneCovs()
        
    def loadSeq(self,seq):
        """Load a fasta file and return a dict with the ID as the key."""
        seqs=SeqIO.parse(open(seq,'rt'),'fasta')
        chrom={}
        for seq in seqs:
            chrom[seq.id]=seq.seq
        return chrom

    def loadGenes(self):
        """Load bed file gens and save to self.chrom_genes as dict."""
        df=pd.read_csv(self.geneBed,sep='\t')
        self.chrom_genes=df.set_index('gene').to_dict(orient='index')

    def loadMuts(self):
        """Load TSV file mutations and save to self.mutations as dict."""
        df=pd.read_csv(self.muts,sep='\t')
        self.mutations=df.groupby('gene')['Mutations'].apply(lambda g: list( g.values)).to_dict()


    def runMuts(self,gene,prot):
        """Check gene mutations against list of mutations save to self.savemuts list."""
        muts=self.mutations
        for mut in muts[gene]:
            if mut.split(' ')[0] == 'ins': continue
            wt=mut[0] # wild type residue
            m=mut[-1] # expected mutant residue (EXP)
            aa=int(mut[1:-1]) # AA position in gene 
            r = prot[aa-1] # sample residue
            sub = '{0}{1}{2}'.format(wt,aa,r)
            st=False
            if r == m: st=[gene,'Mutation',aa, wt, r,m]
            elif r != m and r != wt: 
                if sub not in muts[gene]:
                    st=[gene,'New Mutation',aa, wt, r,m]
            elif r == wt: st=[gene,'Wild type',aa, wt,r,m]
            if st != False:
                self.savemuts.append(st)

    def runGenes(self):
        """Get genes from consensus seqs, translate it needed and check mut positions."""
        self.seqs={}
        muts=self.mutations
        for gene in self.chrom_genes:
            if self.chrom_genes[gene]['chrom'] not in self.chrom: continue
            chrom=self.chrom_genes[gene]['chrom']
            s=self.chrom[chrom][self.chrom_genes[gene]['start']-1:self.chrom_genes[gene]['stop']]
            self.seqs[gene]=s
            self.chrom_genes[gene]['seq']=s
            ## translate proteins
            if self.chrom_genes[gene]['score']==1:
                if self.chrom_genes[gene]['strand'] in ['+',1]:
                    self.chrom_genes[gene]['prot']=s.translate()
                elif self.chrom_genes[gene]['strand'] in ['-',-1]:
                    self.chrom_genes[gene]['prot']=s.reverse_complement().translate()
            elif self.chrom_genes[gene]['score']==0:
                if self.chrom_genes[gene]['strand'] in ['+',1]:
                    self.chrom_genes[gene]['prot']=s
                elif self.chrom_genes[gene]['strand'] in ['-',-1]:
                    self.chrom_genes[gene]['prot']=s.reverse_complement()
            if gene in muts:
                self.runMuts(gene,self.chrom_genes[gene]['prot'])



    def writeMuts(self):
        with open('{0}_{1}_checkMuts.csv'.format(self.samplename,self.refname),'wt') as outf:
            s='{0},{1},{2},{3},{4},{5},{6}\n'.format('sample name',
                    'gene',
                    'mut type',
                    'AA pos',
                    'WT res',
                    'res',
                    'EXP res',)
            for i in self.savemuts:
                s+='{0},{1},{2},{3},{4},{5},{6}\n'.format(self.samplename,
                        i[0],
                        i[1],
                        i[2],
                        i[3],
                        i[4],
                        i[5])
            outf.write(s)

    def geneCovs(self):
        df=pd.read_csv(self.psf,sep='\t')
        for g in self.chrom_genes:
            gene=self.chrom_genes[g]
            if gene['chrom'] not in self.refs: continue
            gdf=df[df['chrom']==gene['chrom']]
            gdf=gdf[gdf['pos']>=min([gene['start'],gene['stop']])]
            gdf=gdf[gdf['pos']<=max([gene['start'],gene['stop']])]
            gene['mean coverage']=gdf['reads_all'].mean()
            gene['minimun coverage']=gdf['reads_all'].min()
            gene['maximun coverage']=gdf['reads_all'].max()
        df=pd.DataFrame(self.chrom_genes)
        df=df.T
        df.reset_index()
        df['sample name']=self.samplename
        df=df[df['chrom'].isin(self.refs)]
        cols=['chrom','start','stop','score','strand','seq','prot','mean coverage',
            'minimun coverage','maximun coverage','sample name']
        for col in cols:
            if col not in df.columns:
                df[col]=None
        df.to_csv('{0}_{1}_geneCovs.csv'.format(self.samplename,self.refname))
        return df

                



def runCheckMuts(opts):
    cm=checkMuts(opts.sample,opts.ref,opts.samplename,opts.refname,opts.bed,opts.muts,opts.pysamstatsfile)
    cm.run()

def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=True,
            help='aligned conensus file name')
    parser.add_argument('-r','--ref',required=True,
            help='reference fasta file')
    parser.add_argument('-b','--bed',required=True,
            help='reference bed file for genes')
    parser.add_argument('-m','--muts',required=True,
            help='reference mutations file for genes')
    parser.add_argument('-sn','--samplename',required=True,
            help='sample name')
    parser.add_argument('-rn','--refname',required=True,
            help='reference name')
    parser.add_argument('-p','--pysamstatsfile',required=True,
            help='pysamstatsfile')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='Check mutations from panel and a reference genome')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runCheckMuts(opts)

