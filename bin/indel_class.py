#!/usr/bin/env python3
import pysam
from argparse import ArgumentParser
import pandas as pd

bases=['A','T','C','G']

def getPosInfo(bam, chrom, pos):
    samfile = pysam.AlignmentFile(bam, "rb" )
    refs=samfile.references    
    n,b=0,[]
    if chrom in refs:
            for pileupcolumn in samfile.pileup(chrom, int(pos)-10000, int(pos)+10000):
                if pileupcolumn.pos != int(pos): continue
                for pileupread in pileupcolumn.pileups:
                    n+=1
                    if pileupread.indel != 1: continue
                    b.append(pileupread.alignment.query_sequence[pileupread.query_position+1])
    
    samfile.close()
    return n,b


def run_pos(bam, chrom, pos, sample):
    reads,bases=getPosInfo(bam,chrom,pos)
    if reads > 0:
        A=bases.count('A')
        AP=(A/reads)*100
        T=bases.count('T')
        TP=(T/reads)*100
        C=bases.count('C')
        CP=(C/reads)*100
        G=bases.count('G')
        GP=(G/reads)*100
        print(chrom, pos, reads, AP, TP, CP, GP , sample, sep='\t')
        return AP, TP, CP, GP
    else:
        return None

def getINDELs(r):
    if 'ins' in r['Mutations'] or 'del' in r['Mutations']:
        r['indel']=True
    else:
        r['indel']=False
    return r

def getPOS(r):
    m=r['Mutations']
    mut_pos=int(m.split(' ')[-1][:-1])
    r['pos']=r['start']+mut_pos
    r['mut pos']=mut_pos
    r['WT res']=m.split(' ')[0]
    r['EXP res']=m.split(' ')[-1][-1]
    return r

def interpret(t, wt, e, pa):
    i_max = max(range(len(t)), key=t.__getitem__)
    if t[i_max] >= 30:
        res=bases[i_max]
        if res==e and pa=='present':
            mut_type='Mutation'
        elif res!=e and pa=='present':
            mut_type='New Mutation'
        elif res==e and pa=='absent':
            mut_type='Wild type'
    else:
        if pa=='present':
            res=wt
            mut_type='Wild type'
        elif pa=='absent':
            res=wt
            mut_type='Mutation'
    return mut_type, res

def run(opts):
    df=pd.read_csv(opts.mutations,sep='\t')
    df=df.apply(getINDELs,axis=1)
    df=df[df['indel']==True] 
    bdf=pd.read_csv(opts.bed_file,sep='\t')
    df=df.merge(bdf, on='gene', how='left')
    df=df.apply(getPOS,axis=1)
    s="sample name,gene,mut type,AA pos,WT res,res,EXP res\n"
    print('Chrom', 'pos', 'Reads','%A','%T','%C','%G','sample', sep='\t')
    for i,r in df.iterrows():
        t=run_pos(opts.bam, r['chrom'], r['pos'], opts.strain)
        if t!=None:
            mut_type,res=interpret(t, r['WT res'], r['EXP res'], r['presence/absence'])
            s+=f"{opts.strain},{r['gene']},{mut_type},{r['mut pos']},{r['WT res']},{res},{r['EXP res']}\n"   
    with open(opts.output,'wt') as outf:
        outf.write(s)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='determine positional INDEL in bam file')
    parser.add_argument('-b', '--bam', required=True,
                             help='Input bam file')
    parser.add_argument('-muts', '--mutations', required=True,
                             help='Input mutations')
    parser.add_argument('-bed', '--bed_file', required=True,
                             help='bed file of gene positions')
    parser.add_argument('-o', '--output', required=True,
                             help='output csv file for analysis')
    parser.add_argument('-s', '--strain', required=False,default=None,
                             help='strain information')
    parser.add_argument('-m', '--model', required=False,default=None,
                             help='model information')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
