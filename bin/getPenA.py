#!/usr/bin/env python3
import sys, os, gzip
from argparse import ArgumentParser
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline, NcbiblastpCommandline
from io import StringIO
import pandas as pd
import re


def getMuts(hsp):
    p=hsp.match
    a=[m.start() for m in re.finditer(' ', p)]
    a.extend([m.start() for m in re.finditer('\+', p)])
    a.sort()
    if len(a) == 0:
        return
    q=hsp.query
    s=hsp.sbjct
    muts=[]
    for pos in a:
        m='{0}{1}{2}'.format(s[pos],pos,q[pos])
        muts.append(m)
    return ' '.join(muts)

def getAllele(r):
    s=r.replace('penA_','').split('_')[0]
    return float(s)

def stripName(r):
    return r[:-2].replace('_','')

def getNuclBlast(fn):
    headers=['qseqid', 'sseqid', 'pident', 'length', 'mismatch',\
                        'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']
    df=pd.read_csv(fn, names=headers, sep='\t')
    maxScore=df['bitscore'].max()
    df=df[df['bitscore'] >= maxScore]
    return df

def getPenA(fp,fn, meta, sample_name='not given',sub=None,oper=None):
    
    matchDict = {}
    blast_records = NCBIXML.parse( open(fp,'rt') )
    dfs=[]
    for record in blast_records:
        if len(record.alignments)==0:continue
        for alignment in record.alignments:
            genotypeName = str(alignment.title.split()[1])
            matchLength = alignment.hsps[0].identities
            #genotypeName, matchLength
            matchDict.setdefault(matchLength,[]).append(genotypeName)
            muts=getMuts(alignment.hsps[0])

            d={'allele':genotypeName,
                    'matchLength':matchLength,
                    'score':alignment.hsps[0].score,
                    'bits':alignment.hsps[0].bits,
                    'muts':muts}
            df=pd.DataFrame(d,index=[0])
            dfs.append(df)

    df=pd.concat(dfs)
    maxScore=df['score'].max()
    df=df[df['score'] >= maxScore]
    df['Allele Type'] = df['allele'].map(getAllele)
    df['allele']=df['allele'].map(stripName)
    
    meta=pd.read_excel(meta)
    df=df.merge(meta,on=['Allele Type'], how='left')
    df['sample_name']=sample_name
    df['oper']=oper
    df['sub']=sub
    df=df[['sample_name','allele','Allele Type','AMR Markers','Beta-Lactamase',
        'score','bits','muts','matchLength','oper','sub']]

    df2=getNuclBlast(fn)
    df=df.merge(df2, left_on=['allele'], right_on=['sseqid'], how='left')
    df=df[~df['sseqid'].isna()]


    df.to_csv('{0}_{1}_{2}_penA_results.csv'.format(sample_name, sub, oper),index=False)


if __name__ == "__main__":
    parser = ArgumentParser(description='get penA alleles from protein seqs')
    parser.add_argument('-b', '--bp5', required=True,
                                         help='protein blastp file')
    parser.add_argument('-n', '--bn6', required=True,
                                         help='nucleotide blastn file')
    parser.add_argument('-m', '--meta', required=True,
                                         help='ngstar meta data')
    parser.add_argument('-s', '--sample_name', required=True,
                                         help='sample name')
    parser.add_argument('-d', '--depth', required=False,
                                         help='sub sample depth')
    parser.add_argument('-op', '--oper', required=False,
                                         help='method used')
    opts, unknown_args = parser.parse_known_args()

    getPenA(opts.bp5,
            opts.bn6,
            opts.meta,
            sample_name=opts.sample_name,
            sub=opts.depth,
            oper=opts.oper)
