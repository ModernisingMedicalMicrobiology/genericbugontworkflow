#!/usr/bin/env python3
import sys, os, gzip
from argparse import ArgumentParser
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline, NcbiblastpCommandline
from io import StringIO


def getRefVariants(outf,inf,ref,polishedGene,sampleName):
    DEBUG = True
    
    o = open(outf, 'a')
    
    refFile = ref
    refFa = SeqIO.read(refFile, 'fasta')
    # relative to ref geneome - http://www.ncbi.nlm.nih.gov/nuccore/NC_011035.1
    
    #list of genes to extract
    # gene: name, start, end, drug
    #number starting from one, will convert to python form in script: gene, start, end, strand, antibiotic
    geneList = [
                #['penA', 1524397, 1526148, -1,'cfx'], 
                #['penB', 2049351, 2050397, -1,'cfx'], 
                ['mtrR', 1332867, 1333499, 1,'cro'], 
                ['pilQ', 103068, 105263, -1,'cro'], 
                ['ponA', 108362, 110758, 1,'cro'],
                ['mtrR', 1332867, 1333499, 1,'cfx'], 
                ['pilQ', 103068, 105263, -1,'cfx'], 
                ['ponA', 108362, 110758, 1,'cfx'],
                ['gyrA', 1051396, 1054146, 1, 'cip'], 
                #['gyrB', 2098152, 2100542, -1, 'cip'], 
                ['parC', 193663, 195966, -1, 'cip'], #,
                #['parE', 1296483, 1298468, -1, 'cip'],
                ['norM_promoter', 460253, 460253, 0, 'cip'],
                ['rpsJ', 2031311, 2031622, 1, 'tet'],
                ['pilQ', 103068, 105263, -1,'tet'],
                ['mtrR', 1332867, 1333499, 1,'tet'],
                ['mtrR', 1332867, 1333499, 1,'pen'], 
                ['pilQ', 103068, 105263, -1,'pen'], 
                ['ponA', 108362, 110758, 1,'pen'],
                ['mtrR', 1332867, 1333499, 1,'azt'],
                ['macAB_promoter', 1415365, 1415365, 0, 'azt'],
                ['23S',1725642, 1725642, 0, 'azt'],
                ['23S',1621743, 1621743, 0, 'azt'],
                ['23S',1264255, 1264255, 0, 'azt'],
                ['23S',1958532, 1958532, 0, 'azt']
                ]
    
    #get list of known AA variants, except mtr promoter which analyses nucleotides
    #http://cmr.asm.org/content/27/3/587.full.pdf
    
    knownSNPs = dict()
    knownSNPs['cro'] = dict()
    #do from de novo
    #knownSNPs['cfx']['penA'] = [311, 312, 316, 483, 501, 512, 542, 545, 551] 
    knownSNPs['cro']['penB'] = [120, 121] #G120K and G120D/A121D
    knownSNPs['cro']['mtrR'] = [45, 39] #G45D A39T
    #mtr - promoter - deletion - nucleotide
    knownSNPs['cro']['pilQ'] = [666] #E666K
    knownSNPs['cro']['ponA'] = [421] #L421P
    
    knownSNPs['cfx'] = dict()
    #do from de novo
    #knownSNPs['cfx']['penA'] = [311, 312, 316, 483, 501, 512, 542, 545, 551] 
    knownSNPs['cfx']['penB'] = [120, 121] #G120K and G120D/A121D
    knownSNPs['cfx']['mtrR'] = [45, 39] #G45D A39T
    #mtr - promoter - deletion - nucleotide
    knownSNPs['cfx']['pilQ'] = [666] #E666K
    knownSNPs['cfx']['ponA'] = [421] #L421P
    
    knownSNPs['pen'] = dict()
    knownSNPs['pen']['mtrR'] = [39, 45] #G45D
    knownSNPs['pen']['pilQ'] = [666] #E666K
    knownSNPs['pen']['ponA'] = [421] #L421P
    
    
    knownSNPs['cip'] = dict()
    knownSNPs['cip']['gyrA'] = [91, 95] #['S91F', 'D95N', 'D95G'] 
    knownSNPs['cip']['parC'] = [86, 87, 88, 91] #['D86N', 'S88P', 'E91K']
    knownSNPs['cip']['norM_promoter'] = [1] #SNP C to T
    
    knownSNPs['tet'] = dict()
    knownSNPs['tet']['mtrR'] = [39,45] #G45D
    knownSNPs['tet']['pilQ'] = [666] #E666K
    knownSNPs['tet']['rpsJ'] = [57] #V57M
    
    knownSNPs['azt'] = dict()
    knownSNPs['azt']['mtrR'] = [39, 45] #G45D, A39T
    knownSNPs['azt']['macAB_promoter'] = [1] #SNP C to A
    knownSNPs['azt']['23S'] = [0]
    
    f = open( inf )
    fa = SeqIO.read( f, 'fasta' )
    if DEBUG: sys.stdout.write('Mapped file: %s\n'%inf)
    
    for gene in geneList:
        if gene[0] != polishedGene: continue
        if DEBUG: sys.stdout.write('Gene: %s\n'%gene[0])
        strand = gene[3]
        abx = gene[4]
        
        if strand == 1:
            proteinSeq = fa[gene[1]-1:gene[2]-1].seq.translate()
            refProteinSeq = refFa[gene[1]-1:gene[2]-1].seq.translate()
        elif strand == -1:
            proteinSeq = fa[gene[1]:gene[2]].seq.reverse_complement().translate()
            refProteinSeq = refFa[gene[1]:gene[2]].seq.reverse_complement().translate()
        elif strand ==0: #DNA sequence
            base = fa[gene[1]]
        
        for site in knownSNPs[abx][gene[0]]:
            if abs(strand)==1: 
                aa = proteinSeq[site-1]
            else:
                aa = base
                site = gene[1]
            outputString = '%s\t%s\t%s\t%s\t%s\n'%(sampleName, abx, gene[0], site, aa)
            if DEBUG: sys.stdout.write(outputString)
            o.write(outputString)
    
    o.close()


inf=sys.argv[1]
ref=sys.argv[2]
outf=sys.argv[3]
gene=sys.argv[4]
sampleName=sys.argv[5]
getRefVariants(outf,inf,ref,gene,sampleName)
