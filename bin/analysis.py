#!/usr/bin/env python3
import sys
import json
from argparse import ArgumentParser
import pandas as pd
from pandas import Series

class analysis:
    def __init__(self,samplename,determinants,variants,mode,
            geneCovs=False,
            minDepth=10,
            genomeDepths=False):
        self.sampleName=samplename
        self.determinants=determinants
        self.variants=variants
        self.mode=mode
        self.geneCovs=geneCovs
        self.minDepth=minDepth
        self.genomeDepths=genomeDepths

    def run(self):
        self.loadDeterminants()
        self.loadResults()
        if self.mode=='chromosome':
            if self.geneCovs!=False:
                self.loadGeneCovs()
            else:
                print('Need geneCovs for chromosome analysis')
                sys.exit()

            self.compare()
            self.predictSR()
            self.saveResults()
        if self.mode=='mobile':
            self.compareMobile()
            self.predictSRmobile()
            self.saveResults()

    def loadDeterminants(self):
        self.df=pd.read_csv(self.determinants,sep='\t')

    def loadResults(self):
        self.results=pd.read_csv(self.variants)

    def compareMobile(self):
        df=self.results.merge(self.df,left_on='chrom', right_on='Gene', how='left')
        self.results=df[df['covBreadth1x']>=0.9]
        self.abx=self.results[['run name','Antimicrobial agent(s)']]
        self.abx.rename(columns={'run name':'sample name',
            'Antimicrobial agent(s)':'Antimicrobial agent'},inplace=True)
        if len(self.abx)<1:
            return
        s=self.abx['Antimicrobial agent'].str.split(' and ').apply(Series, 1).stack()
        s.index = s.index.droplevel(-1)
        s.name = 'Antimicrobial agent' #self.sampleName
        self.abx=self.abx[['sample name']]
        self.abx=self.abx.join(s)
        self.abx=self.abx[['sample name','Antimicrobial agent']]

    def loadGeneCovs(self):
        self.geneCovsDF=pd.read_csv(self.geneCovs,index_col=0)
        self.geneCovsDF.index.name = 'Gene'
        self.geneCovsDF.reset_index(inplace=True)

    def compare(self):
        # merge variants found with list of determinants 
        df=self.results.merge(self.df,left_on='gene', 
                right_on='Gene', 
                how='inner')
        # filter out unknown mutations, keep known mutations
        self.results=df[df['mut type']=='Mutation']
        # create list of resistant ABX
        self.abx=self.results[['sample name','Antimicrobial agent']]

    def predictRow(self,r):
        if r['mut type'] == 'Mutation' and self.minDepth and r['minimun coverage'] >= self.minDepth:
            r['SR genotype'] = 'R'
        elif r['mut type'] != 'Mutation':
            if r['mean coverage'] >= self.minDepth and r['minimun coverage'] >= self.minDepth:
                r['SR genotype'] = 'S'
            else:
                r['SR genotype'] = 'U'
        else:
            r['SR genotype'] = 'U'
        return r

    def predictSR(self):
        self.sr=self.df.merge(self.geneCovsDF,
                left_on='Gene', 
                right_on='Gene', 
                how='inner')
        self.sr=self.sr.merge(self.results, left_on='Gene',
                right_on='Gene',
                how='left')
        self.sr=self.sr.apply(self.predictRow, axis=1)
        self.sr.drop_duplicates(inplace=True)
        cols=['Antimicrobial agent_x','Gene','SR genotype',
                'mean coverage', 'AA pos',
                'WT res',
                'res',
                'EXP res']
        for col in cols:
            if col not in self.sr:
                self.sr[col]=None
        self.sr=self.sr[cols]

    def predictRowMobile(self,r):
        if r['covBreadth10x'] >= 0.9:
            r['SR genotype'] = 'R'
        elif self.genomeDepthCov['covBreadth10x'].max() >= 0.8:
            r['SR genotype'] = 'S'
        else:
            r['SR genotype'] = 'U'
        return r

    def predictSRmobile(self):
        self.sr=self.df.merge(self.results, left_on='Gene',
                right_on='chrom', how='left')
        self.genomeDepthCov=pd.read_csv(self.genomeDepths)
        self.sr=self.sr.apply(self.predictRowMobile,axis=1)
        cols=['Antimicrobial agent(s)_x',
                'Gene_x',
                'SR genotype']
        self.sr=self.sr[cols]
        print(self.sr)

    def saveResults(self):
        self.results.to_csv('{0}_{1}_results.csv'.format(self.sampleName,self.mode),
                index=False)
        self.abx.to_csv('{0}_{1}_abx.csv'.format(self.sampleName,self.mode),
                index=False)
        self.sr.to_csv('{0}_{1}_SR.csv'.format(self.sampleName,self.mode),
                index=False)


def runAnalysis(opts):
    a=analysis(opts.sample,
            opts.determinants,
            opts.input,
            opts.mode,
            geneCovs=opts.gene_covs,
            minDepth=opts.minDepth,
            genomeDepths=opts.genomeDepths)
    a.run()

def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=False,default='unknown sample',
            help='sample name')
    parser.add_argument('-d','--determinants',required=True,
            help='determinant file')
    parser.add_argument('-i','--input',required=False,
            help='chromosomal or mobile variants')
    parser.add_argument('-m','--mode',required=True,
            help='chromosomal or mobile')
    parser.add_argument('-c','--gene_covs',required=False,default=False,
            help='chromosomal gene coverage file')
    parser.add_argument('-g','--genomeDepths',required=False,default=False,
            help='chromosomal genome coverage file')
    parser.add_argument('-x','--minDepth',required=False,default=10,
            help='minimum coverage depth required')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='predict AMR profile from determinants file and results')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runAnalysis(opts)

