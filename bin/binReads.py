#!/usr/bin/env python3
import sys
from argparse import ArgumentParser, SUPPRESS
#from pymongo import MongoClient
from ete3 import NCBITaxa
from Bio import SeqIO
import gzip
import os
ncbi = NCBITaxa()

def getDescendants(taxid):
    dnts = ncbi.get_descendant_taxa(taxid, intermediate_nodes=True)
    dnts.append(int(taxid))
    return dnts

def getSpecies(taxes,level='species'):
    l=ncbi.get_lineage(taxes)
    r=ncbi.get_rank(l)
    return {level:i for i in l if r[i] == level}

class binner:
    def __init__(self,dbname,barcode,mdbip='127.0.0.1',mdbport=27017,cf=None):
        self.cf=cf
        self.dbname=str(dbname)
        self.barcode=str(barcode)
        if self.cf == None:
            client = MongoClient(mdbip, mdbport)
            self.db = client[dbname]
        self.barcodes={}

    def _iterFile(self):
        if self.cf.endswith('gz'):
            mo = gzip.open
        else:
            mo = open
        header=['read_id','seqID','taxID','score','s2ndBestScore','hitLength','queryLength','numMatches']
        for line in mo(self.cf,'rt'):
             l=line.split('\t')
             if l[0]=="readID" or l[0]=="read_id": 
                 header=l
                 header[0]='read_id'
             else:
                 yield {header[i]:l[i] for i in range(len(l))}
 
    def allStats(self):
        if self.cf == None:
            print('from DB')
            collection = self.db.cent_stats
            hce = collection.find()
            return hce
        else:
            print('from centrifuge file')
            hce = self._iterFile()
            return hce

    def _fq(self,fqs):
        for fq in fqs:
            if fq.endswith('.gz'):
                mo=gzip.open
            else:
                mo=open
            for seq in SeqIO.parse(mo(fq,'rt'),'fastq'):
                if seq.id in self.readsTaxids:
                    yield seq

    def binFq(self,fqs,taxids):
        handles = dict((t,gzip.open('bins/{0}_{2}_{1}.fq.gz'.format(self.dbname,t,self.barcode), 'wt')) for t in taxids)
        ifq=self._fq(fqs)

        if len(taxids)==1:
            SeqIO.write(ifq, handles[taxids[0]], 'fastq')

        else:
            for t in taxids:
                handles[t].write(seq.format('fastq'))

    def binFqs(self,fq1,fq2,taxids):
        for t in taxids:
            if not os.path.exists('bins/{0}_{1}/'.format(self.dbname,t)):
                os.makedirs('bins/{0}_{1}/'.format(self.dbname,t))
        handles1 = dict((t,gzip.open('bins/{0}_{1}/{0}_{1}.r1.fq.gz'.format(self.dbname,t), 'wt')) for t in taxids)
        handles2 = dict((t,gzip.open('bins/{0}_{1}/{0}_{1}.r2.fq.gz'.format(self.dbname,t), 'wt')) for t in taxids)	
        for seq1,seq2 in zip(SeqIO.parse(open(fq1[0],'rt'),'fastq'),SeqIO.parse(open(fq2,'rt'),'fastq')):
            for t in taxids:
                if seq1.id.split('/')[0] in self.reads[t]:
                    handles1[t].write(seq1.format('fastq'))
                    handles2[t].write(seq2.format('fastq'))


    def binTax(self,taxids):
        t,self.reads={},{}
        for taxid in taxids: t[taxid]=getDescendants(taxid) # dict of descendents
        rt=dict((v, k) for k in t for v in t[k]) # reverse of tax dictionary
        cents=self.allStats()
        self.readsTaxids={}
        for i in cents:
            if int(i['taxID']) in rt:
                self.reads.setdefault(rt[int(i['taxID'])],set()).add(i['read_id'])
                self.readsTaxids[i['read_id']]=rt[int(i['taxID'])]


    def topHits(self,b='nobarcode',level='species'):
        self.barcodes[b]={'Viruses':{'bases':0,'reads':0,'spBases':{}}}
        cents=self.allStats()
        #taxes={"Bacteria":2,"Human":9606,"Viruses":10239}
        taxes={"Viruses":10239}
        self.kingdoms={}
        for k in taxes:self.kingdoms[k]=getDescendants(taxes[k])
        self.tax2kingdom = dict((v, k) for k in self.kingdoms for v in self.kingdoms[k])
        for i in cents:
             if int(i['taxID']) in self.tax2kingdom:
                self.barcodes[b][self.tax2kingdom[int(i['taxID'])]]['bases']+=int(i['queryLength'])
                self.barcodes[b][self.tax2kingdom[int(i['taxID'])]]['reads']+=1
                if self.tax2kingdom[int(i['taxID'])]=='Viruses':
                    sd=getSpecies(int(i['taxID']),level=level)
                    if level in sd:
                        if sd[level] not in self.barcodes[b]['Viruses']['spBases']:
                            self.barcodes[b]['Viruses']['spBases'][sd[level]]={}
                            self.barcodes[b]['Viruses']['spBases'][sd[level]]['bases']=0
                            self.barcodes[b]['Viruses']['spBases'][sd[level]]['reads']=0
                        self.barcodes[b]['Viruses']['spBases'][sd[level]]['bases']+=int(i['queryLength'])
                        self.barcodes[b]['Viruses']['spBases'][sd[level]]['reads']+=1
def run(opts):
    ## run
    if not os.path.exists('bins'):
        os.makedirs('bins')
    taxes=list(map(int,opts.taxes.split(',')))
    m=binner(opts.sample_name,opts.barcode,mdbip=opts.ip,mdbport=opts.port,cf=opts.cf)
    if opts.taxes == None:
        m.topHits(level=opts.level)

        for b in m.barcodes: m.barcodes[b]['Viruses']['filtspecies']={k: v for k, v in m.barcodes[b]['Viruses']['spBases'].items() if v['bases'] > opts.bases}
        taxes=list(m.barcodes['nobarcode']['Viruses']['filtspecies'].keys())
        m.binTax(taxes)
    else: 
        m.binTax(taxes)
    taxes=list(m.reads.keys())
    if opts.fastq2 == None: m.binFq(opts.fastq,taxes)
    else: m.binFqs(opts.fastq,opts.fastq2,taxes)


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Read binner for binning centrifuge reads')
    parser.add_argument('-s', '--sample_name', required=True,
                             help='Specify sample name will be used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                             help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                             help='port address for mongoDB database')
    parser.add_argument('-fq','--fastq',required=True,nargs='+',
                             help='Fastq file for binning')
    parser.add_argument('-fq2','--fastq2',required=False,default=None,
                             help='reverse Fastq file for binning')
    parser.add_argument('-b', '--bases', required=False, default=4000,type=int,
                             help='number of bases needed to be binned')
    parser.add_argument('-l', '--level', required=False, default='species',type=str,
                             help='taxominc level to bin at, default=species')
    parser.add_argument('-cf','--cf',required=False,default=None,
                             help='centrifuge file')
    parser.add_argument('-tax','--taxes',required=False,default=None,
                             help='list of taxids, comma sep')
    parser.add_argument('-br', '--barcode', required=False,default='',
                             help='barcode name')

    opts, unknown_args = parser.parse_known_args()

    #run
    run(opts)


