#!/usr/bin/env python3
import sys
from argparse import ArgumentParser
import pandas as pd
from pandas import Series

class report:
    def __init__(self,samplename,mobile,chrom,depths,output):
        self.sampleName=samplename
        self.output=output
        self.mobile = mobile
        self.chrom = chrom
        self.depths = depths

    def run(self):
        self.mobile=self.loadFile(self.mobile)
        self.chrom=self.loadFile(self.chrom)
        self.depths=self.loadFile(self.depths)
        self.combineResults()
        self.saveReport()

    def loadFile(self,f):
        return pd.read_csv(f)

    def overWriteSens(self, sr):
        if 'R' in sr:
            return 'R'
        elif 'S' in sr:
            return 'S'
        elif 'U' in sr:
            return 'U'
        else:
            return 'S'

    def combineResults(self):
        self.chrom['Antimicrobial agent']=self.chrom['Antimicrobial agent_x']
        self.mobile['Antimicrobial agent']=self.mobile['Antimicrobial agent(s)_x']
        cols=['Antimicrobial agent',
                'SR genotype']
        self.chrom=self.chrom[cols]
        self.mobile=self.mobile[cols]

        df=pd.concat([self.mobile,self.chrom])
        # explode multiple abx into new rows
        df.rename(columns={'Antimicrobial agent':'abx'},inplace=True)
        df=df.assign(abx=df['abx'].str.split(',')).explode('abx')

        # check if multiple determinants have R amongst S
        self.report=df.groupby('abx')['SR genotype'].apply(lambda x: ''.join(x)).reset_index()
        self.report['SR genotype']=self.report['SR genotype'].map(self.overWriteSens)

        self.report['sample name']=self.sampleName

        self.report=self.report.pivot(index='sample name',
                columns='abx', values='SR genotype')
        
        self.report=self.depths.merge(self.report, right_on='sample name',
                left_on='run name', how='left')

    def saveReport(self):
        self.report.to_csv(self.output,
                sep='\t',
                index=False)


def runAnalysis(opts):
    r=report(opts.sample,
        opts.mobile,
        opts.chrom,
        opts.depths,
        opts.output)
    r.run()



def compareGetArgs(parser):
    parser.add_argument('-s','--sample',required=False,default='unknown sample',
            help='sample name')
    parser.add_argument('-m','--mobile',required=True,
            help='mobile SR analys')
    parser.add_argument('-c','--chrom',required=True,
            help='chromosome SR analysis')
    parser.add_argument('-d','--depths',required=True,
            help='coverage depths')
    parser.add_argument('-o','--output',required=True,
            help='output tsv file')
    return parser

if __name__ == '__main__':
    parser = ArgumentParser(description='predict AMR profile from determinants file and results')
    parser = compareGetArgs(parser)

    opts, unknown_args = parser.parse_known_args()
    # run
    runAnalysis(opts)

